<?php

//namespace DataBase;
require_once 'db.php';

/**
 * 
 * @author Admin
 */
class GroupInvestor {

    const USER_TYPE = "2.2";

    private $id;
    private $user_id;
    private $user_type;
    private $group_name;
    private $description;
    private $address;
    private $city;
    private $state;
    private $country;
    private $zip_code;
    private $fb_profile;
    private $ln_profile;
    private $url;
    private $type_total_fund;
    private $total_fund;
    private $type_of_investment;
    private $type_investment_size;
    private $investment_size;
    private $other_criteria;
    private $investment_areas;
    private $profile_image;

    function __construct($groupInvestor = array()) {
        $this->id = isset($groupInvestor['id']) ? $groupInvestor['id'] : null;
        $this->user_id = isset($groupInvestor['user_id']) ? $groupInvestor['user_id'] : null;
        $this->user_type = isset($groupInvestor['user_type']) ? $groupInvestor['user_type'] : null;
        $this->group_name = isset($groupInvestor['group_name']) ? $groupInvestor['group_name'] : null;
        $this->description = isset($groupInvestor['description']) ? $groupInvestor['description'] : null;
        $this->address = isset($groupInvestor['address']) ? $groupInvestor['address'] : null;
        $this->city = isset($groupInvestor['city']) ? $groupInvestor['city'] : null;
        $this->state = isset($groupInvestor['state']) ? $groupInvestor['state'] : null;
        $this->country = isset($groupInvestor['country']) ? $groupInvestor['country'] : null;
        $this->zip_code = isset($groupInvestor['zip_code']) ? $groupInvestor['zip_code'] : null;
        $this->fb_profile = isset($groupInvestor['fb_profile']) ? $groupInvestor['fb_profile'] : null;
        $this->ln_profile = isset($groupInvestor['ln_profile']) ? $groupInvestor['ln_profile'] : null;
        $this->url = isset($groupInvestor['url']) ? $groupInvestor['url'] : null;
        $this->type_total_fund = isset($groupInvestor['type_total_fund']) ? $groupInvestor['type_total_fund'] : null;
        $this->total_fund = isset($groupInvestor['total_fund']) ? $groupInvestor['total_fund'] : null;
        $this->type_of_investment = isset($groupInvestor['type_of_investment']) ? $groupInvestor['type_of_investment'] : null;
        $this->type_investment_size = isset($groupInvestor['type_investment_size']) ? $groupInvestor['type_investment_size'] : null;
        $this->investment_size = isset($groupInvestor['investment_size']) ? $groupInvestor['investment_size'] : null;
        $this->other_criteria = isset($groupInvestor['other_criteria']) ? $groupInvestor['other_criteria'] : null;
        $this->investment_areas = isset($groupInvestor['investment_areas']) ? $groupInvestor['investment_areas'] : null;
        $this->profile_image = isset($groupInvestor['profile_image']) ? $groupInvestor['profile_image'] : null;
    }

    function getJsonData() {
        $var = get_object_vars($this);
        foreach ($var as &$value) {
            if (is_object($value) && method_exists($value, 'getJsonData')) {
                $value = $value->getJsonData();
            }
        }
        return $var;
    }

}

class GroupInvestorController {

    function getByUserId($userId) {
        $query = "SELECT sanp.*, u.user_type FROM sc_angel_network_profile sanp INNER JOIN sc_users u ON u.id = sanp.user_id WHERE sanp.user_id = :userId";
        $db = getDB();
        $stmt = $db->prepare($query);
        $stmt->bindParam("userId", $userId);
        $stmt->execute();
        $groupInvestor = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($groupInvestor) {
            return new GroupInvestor($groupInvestor);
        }
        return null;
    }
    
    function userExists($userId){
        $query = "SELECT user_id FROM sc_angel_network_profile WHERE user_id = :userId";
        $db = getDB();
        $stmt = $db->prepare($query);
        $stmt->bindParam("userId", $userId);
        $stmt->execute();
        $userRegistered = $stmt->fetch(PDO::FETCH_ASSOC);
        if($userRegistered){
            return false;
        }else{
            return true;
        }
    }
    
    function registeGroupInvestor($request,$image_name){
        if($image_name == ""){
            $image_name = null;
        }
        $user_id = $request->post('user_id');
        $group_name = $request->post('group_name');
        $description = $request->post('description');
        $address = $request->post('address');
        $city = $request->post('city');
        $state = $request->post('state');
        $country = $request->post('country');
        $pin = $request->post('pin');
        $fb_link = $request->post('fb_link');
        $ln_link = $request->post('ln_link');
        $url = $request->post('url');
        $type_total_fund = $request->post('type_total_fund');
        $total_fund = $request->post('total_fund');
        $type_investment = $request->post('type_investment');
        $type_investment_size = $request->post('type_investment_size');
        $investment_size = $request->post('investment_size');
        $other_criteria = $request->post('other_criteria');
        $investment_areas = $request->post('investment_areas');
        
        try{
            $sql = "INSERT INTO sc_angel_network_profile (id,user_id,group_name,description,profile_image,address,city,state,country,zip_code,fb_profile,ln_profile,url,type_total_fund,total_fund,type_of_investment,type_investment_size,investment_size,other_criteria,investment_areas,created_date)"
                        . "VALUES (:sc_id,:sc_user_id,:sc_group_name,:sc_description,:sc_profile_image,:sc_address,:sc_city,:sc_state,:sc_country,:sc_pin,:sc_fb_link,:sc_ln_link,:sc_url,:sc_type_total_fund,:sc_total_fund,:sc_type_investment,:sc_type_investment_size,:sc_investment_size,:sc_other_criteria,:sc_investment_areas,:sc_created_date)";
            
            $helper = new Helper();
            $id = $helper->guid4();
            $date = date('Y-m-d H:i:s');
            $db = getDB();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("sc_id", $id);
            $stmt->bindParam("sc_user_id", $user_id);
            $stmt->bindParam("sc_group_name", $group_name);
            $stmt->bindParam("sc_description", $description);
            $stmt->bindParam("sc_profile_image", $image_name);
            $stmt->bindParam("sc_address", $address);
            $stmt->bindParam("sc_city", $city);
            $stmt->bindParam("sc_state", $state);
            $stmt->bindParam("sc_country", $country);
            $stmt->bindParam("sc_pin", $pin);
            $stmt->bindParam("sc_fb_link", $fb_link);
            $stmt->bindParam("sc_ln_link", $ln_link);
            $stmt->bindParam("sc_url", $url);
            $stmt->bindParam("sc_type_total_fund", $type_total_fund);
            $stmt->bindParam("sc_total_fund", $total_fund);
            $stmt->bindParam("sc_type_investment", $type_investment);
            $stmt->bindParam("sc_type_investment_size", $type_investment_size);
            $stmt->bindParam("sc_investment_size", $investment_size);
            $stmt->bindParam("sc_other_criteria", $other_criteria);
            $stmt->bindParam("sc_investment_areas", $investment_areas);
            $stmt->bindParam("sc_created_date", $date);
            $stmt->execute();            
            $db = null;
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function updateGroupInvestor($request,$image_name){        
        
        $id = $request->post('id');
        $user_id = $request->post('user_id');
        $group_name = $request->post('group_name');
        $description = $request->post('description');        
        $address = $request->post('address');
        $city = $request->post('city');
        $state = $request->post('state');
        $country = $request->post('country');
        $pin = $request->post('pin');
        $fb_link = $request->post('fb_link');
        $ln_link = $request->post('ln_link');
        $url = $request->post('url');
        $type_total_fund = $request->post('type_total_fund');
        $total_fund = $request->post('total_fund');
        $type_investment = $request->post('type_of_investment');
        $type_investment_size = $request->post('type_investment_size');
        $investment_size = $request->post('investment_size');
        $other_criteria = $request->post('other_criteria');
        $investment_areas = $request->post('investment_areas');
        
        if($image_name == Null){
            $image_name = $request->post('profile_image');
        }
        
        try{
            $query = "UPDATE sc_angel_network_profile SET group_name = :sc_group_name, description = :sc_description, profile_image = :sc_profile_image, address = :sc_address, city = :sc_city, state = :sc_state, country = :sc_country, zip_code = :sc_pin, fb_profile = :sc_fb_link, ln_profile = :sc_ln_link, url = :sc_url, type_total_fund = :sc_type_total_fund, total_fund = :sc_total_fund,"
                    . "type_of_investment = :sc_type_investment, type_investment_size = :sc_type_investment_size, investment_size = :sc_investment_size, other_criteria = :sc_other_criteria, investment_areas = :sc_investment_areas, updated_date = :sc_update_date"
                    . " WHERE user_id = :sc_user_id";
            
            $date = date('Y-m-d H:i:s');
            $db = getDB();
            $stmt = $db->prepare($query);
            $stmt->bindParam("sc_user_id", $user_id);
            $stmt->bindParam("sc_group_name", $group_name);
            $stmt->bindParam("sc_description", $description);
            $stmt->bindParam("sc_profile_image", $image_name);
            $stmt->bindParam("sc_address", $address);
            $stmt->bindParam("sc_city", $city);
            $stmt->bindParam("sc_state", $state);
            $stmt->bindParam("sc_country", $country);
            $stmt->bindParam("sc_pin", $pin);
            $stmt->bindParam("sc_fb_link", $fb_link);
            $stmt->bindParam("sc_ln_link", $ln_link);
            $stmt->bindParam("sc_url", $url);
            $stmt->bindParam("sc_type_total_fund", $type_total_fund);
            $stmt->bindParam("sc_total_fund", $total_fund);
            $stmt->bindParam("sc_type_investment", $type_investment);
            $stmt->bindParam("sc_type_investment_size", $type_investment_size);
            $stmt->bindParam("sc_investment_size", $investment_size);
            $stmt->bindParam("sc_other_criteria", $other_criteria);
            $stmt->bindParam("sc_investment_areas", $investment_areas);
            $stmt->bindParam("sc_update_date", $date);
            $stmt->execute();       
            
            $db = null;
            return true;
        }catch(PDOException $e){
            return false;
        }
    }

}

?>