<?php

include 'db.php';

require_once 'Helper.php';
require_once 'UsersProfile.php';
require_once 'Slim/Slim.php';
require_once 'lib/swift_required.php';
require_once 'StartUp.php';
require_once 'IndividualInvestor.php';
require_once 'GroupInvestor.php';
require_once 'ServiceProvider.php';


\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim(array("debug"=>true));

$app->get('/test', 'testApi');
$app->post('/signup', 'registerUser');
$app->post('/changePassword', 'changePassword');
$app->post('/login', 'loginUser');
$app->post('/forgotPassword', 'forgotUserPassword');
$app->post('/updateUser', 'updateUserInfo');
$app->post('/updateAngelIndividual', 'updateAngelIndividualProfile');
$app->post('/updateAngelGroup', 'updateAngelGroupProfile');
$app->post('/updateServiceProvider', 'updateServiceProviderprofile');
$app->post('/updateStartup', 'updateStartupProfile');
$app->post('/startupProfile', 'updateStartUpProfile');
$app->post('/userUpdates', 'userPostUpdates');
$app->post('/startupFounder', 'registerStartupFounder');
$app->post('/angelIndividual', 'registerAngelIndividualProfile');
$app->post('/angelGroup', 'registerAngelGroupProfile');
$app->post('/serviceProvider', 'registerServiceProvider');
$app->post('/profileImage', 'getProfileImage');
$app->post('/addProduct', 'addServiceProviderProduct');
$app->post('/invite', 'inviteUsers');


//$app->post('/notes','getAllNotes');
//$app->post('/addNote','insertNote');
//$app->post('/updatekey','updateKey');
//$app->post('/delete','deleteNote');
//$app->post('/addUser','insertUser');

$app->run();

function testApi() {

    $request = \Slim\Slim::getInstance()->request();
    /* $test = $request->get('test');
      $date = strtotime(date('Y-m-d H:i:s'));
      $da = strtotime(date('2016-12-12 09:25:15'));
      echo '{"status":true,"message":' . $date . ',"day":'.$da.'}'; */

    $token = $request->headers->get('Auth-Type');
    echo '{"status":true,"validToken":' . isUserAuthentic($token) . '}';
}

//Check User is Aucthentic or not..
function isUserAuthentic($token) {
    try {
        $db = getDB();
        $stmt = $db->prepare("SELECT token_expire FROM sc_users WHERE token = :sc_token");
        $stmt->bindParam("sc_token", $token);
        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        $db = null;
        if (!empty($data)) {
            if (strtotime($data['token_expire']) < strtotime(date('Y-m-d H:i:s'))) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    } catch (PDOException $e) {
        echo json_encode(array("status" => false, "message" => $e->getMessage()));
    }
}

//Login Process
function loginUser() {
    $app = \Slim\Slim::getInstance();
    $request = $app->request();
    $uname = $request->post('user_name');
    $pwd = $request->post('password');
    $pass = md5($pwd);
    if(isset($uname)&& isset($pwd)) {
        
        $userProfile = new UsersProfile();
        $user_value = $userProfile->userLogin($uname,$pass);
        if ($user_value) {
            $helper = new Helper();
            $new_token = $helper->guid4();
            $userProfile = new UsersProfile();
            $userProfile->updateUserTokenExpire($uname, $new_token);

            $app->response->headers->set('Auth-Type', $new_token);
            $user = getUser($user_value['id'], $user_value['user_type']);

            //check if object is instanceOf class
            if (is_a($user, 'StartUp') || is_a($user, 'IndividualInvestor') || is_a($user, 'GroupInvestor') || is_a($user, 'ServiceProvider')) {
                $user = $user->getJsonData();
            }

            echo json_encode(array("status" => true, "user" => $user, "user_type" => $user_value['user_type'], "message" => "User logged in"));
        } else {
            echo json_encode(array("status" => false, "message" => "wrong username or password"));
        }
    } else {
        echo json_encode(array("status" => false, "message" => "Provide email or password"));
    }
}//end of loginUser()

//Check User exits or not
function isUserExists($email = null, $id = null) {
    try {
        $db = getDB();
        if ($id == null) {
            $stmt = $db->prepare("SELECT id from sc_users WHERE email = :sc_email");
            $stmt->bindParam("sc_email", $email);
        } else {
            $stmt = $db->prepare("SELECT id from sc_users WHERE id = :sc_id");
            $stmt->bindParam("sc_id", $id);
        }

        $stmt->execute();
        $rows = $stmt->fetchAll();
        $db = null;
        $num_rows = count($rows);
        return $num_rows > 0;
    } catch (PDOException $e) {

        return false;
    }
}//end of isUserExists()


//Registration Process
function registerUser() {

    $request = \Slim\Slim::getInstance()->request();    
    $email = $request->post('email');
    
    if (isUserExists($email, null)) {
        echo json_encode(array("status" => false, "email" => $email, "message" => "User email already exists"));
    } else {

        $userProfile = new UsersProfile();
        $registration = $userProfile->userRegister($request);
        if($registration['status'] == true){
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers .= 'From: ' . "\r\n";
            //$isSent = mail($email,'Welcome To Sociocross',' Welcome to Socio Cross. You successfully registered and your password is- '.$registration['password'].' ',$headers);
            $helper = new Helper();
            $isSent = $helper->emailSend($email,$registration['password']);
            //$isSent = true;
            if ($isSent) {
                echo json_encode(array("status" => true, "user_id" => $registration['user_id'], "password" => $registration['password'], "message" => "User added successfully"));
            } else {
                $deleteAcount = $userProfile->deleteUserAcount($registration['user_id']);
                if($deleteAcount){
                    echo json_encode(array("status" => false, "message" => "User not added"));
                }
            }
        }else{
            echo json_encode(array("status" => false, "message" => "User not added"));
        }
    }
}//end registerUser()

//Generate Random password
function random_password($length = 8) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$";
    $password = substr(str_shuffle($chars), 0, $length);
    return $password;
}//end of random_password()

//Change Password
function changePassword() {
    $app = \Slim\Slim::getInstance();
    $request = $app->request();
    $token = $request->headers->get('Auth-Type');
    if (isUserAuthentic($token)) {
        $email = $request->post('email');
        $user_id = $request->post('user_id');
        $old_password = $request->post('old_pwd');
        $new_password = $request->post('pwd');
        $old_pass = md5($old_password);
        $new_pass = md5($new_password);
        $userProfile = new UsersProfile();
        $passChange = $userProfile->passwordChange($user_id, $old_pass, $new_pass);
        if ($passChange) {
            echo json_encode(array("status" => true, "message" => "Password changed"));
        } else {
            echo json_encode(array("status" => false, "message" => "Password not changed"));
        }
    } else {
        echo json_encode(array("status" => false, "message" => "Not Authentic User"));
    }
}//end changePassword()

//Forgot Password functionality 
function forgotUserPassword() {
    $app = \Slim\Slim::getInstance();
    $request = $app->request();
    $usersProfile = new UsersProfile();
    $email = $request->post('email_id');

    if (isUserExists($email, NULL)) {
        $pass = random_password();
        $password = md5($pass);
        $forgotPassword = $usersProfile->passwordForgot($email, $password);
        if ($forgotPassword) {
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers .= 'From: ' . "\r\n";
//            $isSent = mail($email,'Change Password',' Welcome to Socio Cross. Your change password is - '.$pass.' ',$headers);
            $helper = new Helper();
            $helper->emailSend($email,$pass);
            echo json_encode(array("status" => true, "message" => "Your new password has been send to your email id. So check your email.  "));
        } else {
            echo json_encode(array("status" => false, "message" => " Your email id not found"));
        }
    } else {
        echo json_encode(array("status" => false, "message" => "User not exists. Please provide valid email id. "));
    }
}//end forgotUserPassword()

function updateUserInfo() {

    $request = \Slim\Slim::getInstance()->request();

    $user_id = $request->post('user_id');
    //$userType 		= $request->post('user_type');		
    $aboutMe = $request->post('about_me');
    $moreAboutMe = $request->post('more_about_me');
    $address = $request->post('address');
    $city = $request->post('city');
    $state = $request->post('state');
    $country = $request->post('country');
    $zipCode = $request->post('zip_code');
    $fbProfile = $request->post('fb_profile');
    $lnProfile = $request->post('ln_profile');
    $twProfile = $request->post('tw_profile');

    $sql = "UPDATE sc_users SET about_me=:sc_aboutme , 
	more_about_me=:sc_more_aboutme ,
	fb_profile=:sc_fb ,ln_profile=:sc_ln ,tw_profile=:sc_tw ,
	address=:sc_address , city=:sc_city ,state=:sc_state,country=:sc_country,zip_code=:sc_zip ,
	basic_profile=:sc_basic_profile,updated_date=:sf_updated_date where id = :sc_user_id";


    try {

        $date = date('Y-m-d H:i:s');
        $bprofile = "1";
        $db = getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("sc_aboutme", $aboutMe);
        $stmt->bindParam("sc_more_aboutme", $moreAboutMe);
        $stmt->bindParam("sc_fb", $fbProfile);
        $stmt->bindParam("sc_ln", $lnProfile);
        $stmt->bindParam("sc_tw", $twProfile);
        $stmt->bindParam("sc_address", $address);
        $stmt->bindParam("sc_city", $city);
        $stmt->bindParam("sc_state", $state);
        $stmt->bindParam("sc_country", $country);
        $stmt->bindParam("sc_zip", $zipCode);
        $stmt->bindParam("sc_basic_profile", $bprofile);
        $stmt->bindParam("sf_updated_date", $date);
        $stmt->bindParam("sc_user_id", $user_id);
        $stmt->execute();
        $db = null;

        echo json_encode(array("status" => true, "user_id" => $user_id, "message" => "User updated"));
    } catch (PDOException $e) {
        //error_log($e->getMessage(), 3, '/var/tmp/php.log');
        echo json_encode(array("status" => false, "message" => $e->getMessage()));
    }
}

//end updateUserInfo()

function updateUserTypeAndProfileStatus($user_id, $type) {
    $sql = "UPDATE sc_users SET user_type=:sc_user_type,
		profile_updated='1', updated_date=:sc_updated_date 
		where id = :sc_user_id";

    try {

        $db = getDB();
        $date = date('Y-m-d H:i:s');
        $stmt = $db->prepare($sql);
        $stmt->bindParam("sc_user_type", $type);
        $stmt->bindParam("sc_updated_date", $date);
        $stmt->bindParam("sc_user_id", $user_id);
        $stmt->execute();
        $db = null;
        return true;
    } catch (PDOException $e) {

        //error_log($e->getMessage(), 3, '/var/tmp/php.log');
    }

    return false;
}

function updateAngelIndividualProfile() {
    $app = \Slim\Slim::getInstance();
    $request = $app->request();
    $token = $request->headers->get('Auth-Type');    
    $date = date('Y-m-d H:i:s');

    if (isUserAuthentic($token)) {
        $user_id = $request->post('user_id');
        $name = $request->post('name');
        $add = $request->post('address');
        $city = $request->post('city');
        $state = $request->post('state');
        $country = $request->post('country');
        $pin = $request->post('pin');
        $fb_link = $request->post('fb_link');
        $ln_link = $request->post('ln_link');
        $tw_link = $request->post('tw_link');
        $blog_link = $request->post('blog_link');
        $invfrom = $request->post('investing_from');
        $type_budget_amount = $request->post('type_budget_amount');
        $invbudget = $request->post('inv_budget_per_year');
        $type_cap_amount = $request->post('type_cap_amount');
        $invcap = $request->post('inv_cap_per_startup');
        $interest_areas = $request->post('interest_areas');
        $about = $request->post('about');
        $work_exp = $request->post('work_exp');
        $education = $request->post('education');
        
        $data = array($name, $add, $city, $state, $country, $pin, $fb_link, $ln_link, $tw_link, $blog_link, $invfrom,
                        $type_budget_amount, $invbudget, $type_cap_amount, $invcap, $interest_areas, $about, $work_exp, $education, $date, $user_id
                      );

        $valid_value = array(   "name" => $name, "address" => $add,
                                "city" => $city, "state" => $state,
                                "country" => $country, "pin" => $pin,
                                "investing form" => $invfrom, "type of budget amount" => $type_budget_amount,
                                "investment budget per year" => $invbudget, "type of cap amount" => $type_cap_amount,
                                "investment cap per startup" => $invcap, "interest areas" => $interest_areas,
                                "about" => $about, "work experience" => $work_exp,
                                "education" => $education
                            );
        $helper = new Helper();
        $validate = $helper->validation($valid_value);
        if (empty($validate)) {            
            $individualInvestorController = new IndividualInvestorController();
            $update = $individualInvestorController->updateIndividualInvestor($request);
            $deleted_id = $request->post('deleted_id');
            if($deleted_id){
                $delete_list = $individualInvestorController->deleteHistory($request);
            }    
            
            if ($update) {
                $user = getUser($user_id, IndividualInvestor::USER_TYPE);
                $user = $user->getJsonData();

                echo json_encode(array("status" => true, "user" => $user, "message" => "Profile updated successfully"));
            } else {
                echo json_encode(array("status" => false, "message" => "Profile not updated"));
            }
        } else {
            echo json_encode(array("status" => false, "message" => "Provide " . implode(",", $validate)));
        }
    } else {
        echo json_encode(array("status" => false, "message" => "You are not authentic user. Please login !"));
    }
}

//Update Angel Group Profile
function updateAngelGroupProfile() {
    $app = \Slim\Slim::getInstance();
    $request = $app->request();
    $token = $request->headers->get('Auth-Type');    
    $helper = new Helper();
    $date = date('Y-m-d H:i:s');
    $image_name = null;

    if (isUserAuthentic($token)) {
               
        $user_id = $request->post('user_id');
        $group_name = $request->post('group_name');
        $description = $request->post('description');        
        $address = $request->post('address');
        $city = $request->post('city');
        $state = $request->post('state');
        $country = $request->post('country');
        $pin = $request->post('pin');
        $fb_link = $request->post('fb_link');
        $ln_link = $request->post('ln_link');
        $url = $request->post('url');
        $type_total_fund = $request->post('type_total_fund');
        $total_fund = $request->post('total_fund');
        $type_investment = $request->post('type_of_investment');
        $type_investment_size = $request->post('type_investment_size');
        $investment_size = $request->post('investment_size');
        $other_criteria = $request->post('other_criteria');
        $investment_areas = $request->post('investment_areas');
        //    $startup_name           = $request->post('startup_name');
        //    $amount_type            = $request->post('amount_type');
        //    $amount                 = $request->post('amount');
        //echo json_encode(array('status'=>false,"message"=>$type_investment));
        if (isset($_FILES['image'])) {
            $tmp_name = $_FILES['image']['tmp_name'];
            $image_name = explode(".", $_FILES['image']['name']);
            $image_name = ($helper->guid4() . '.' . end($image_name));
        }

        $data = array("group_name"=>$group_name, "description"=>$description, "image_naem"=>$image_name, "address"=>$address, "city"=>$city, "state"=>$state, "country"=>$country, "pin"=>$pin, "fb_link"=>$fb_link, "ln_link"=>$ln_link, "url"=>$url,
                        "type-total_fund"=>$type_total_fund, "total_fund"=>$total_fund, "type_investment"=>$type_investment, $type_investment_size, $investment_size,
                        $other_criteria, $investment_areas, $date, $user_id
                    );
        
        $valid_data = array("group name" => $group_name, "description" => $description,
                            "address" => $address, "city" => $city,
                            "state" => $state, "country" => $country,
                            "zip code" => $pin,
                        );
        $validate = $helper->validation($valid_data);
        if (empty($validate)) {
            try {
                //$result = $updation->angelGroupUpdation($data);
                $groupInvestorController = new GroupInvestorController();
                $update = $groupInvestorController->updateGroupInvestor($request,$image_name);
                 
                if ($update) {
                    if ($image_name != Null) {                  
                        foreach (glob("uploads/AngelGroupInvestment/" . $user_id."/*.*") as $filename) {
                            if (is_file($filename)) {
                                unlink($filename);
                            }
                        }
                         
                        //Create folders in uploads & Save image in folder
                        $path = "uploads/AngelGroupInvestment/$user_id";
                        saveFiles($path, $image_name, $tmp_name);
                    }

                    $user = getUser($user_id, GroupInvestor::USER_TYPE);
                    $user = $user->getJsonData();

                    echo json_encode(array("status" => true, "user" => $user, "message" => "Profile updated successfully"));
                } else {
                    echo json_encode(array("status" => false, "message" => "Profile not updated"));
                }
            } catch (PDOException $e) {
                echo json_encode(array("status" => false, "message" => $e->getMessage()));
            }
        } else {
            echo json_encode(array("status" => false, "message" => "Provide " . implode(",", $validate)));
        }
    } else {
        echo json_encode(array("status" => false, "message" => "You are not authentic user. Please login!"));
    }
}//end updateAngelGroupProfile

//Update Service Provider
function updateServiceProviderprofile() {
    $app = \Slim\Slim::getInstance();
    $request = $app->request();
    $token = $request->headers->get('Auth-Type');
    //$updation = new Updation();    
    $date = date('Y-m-d H:i:s');

    if (isUserAuthentic($token)) {
        $helper = new Helper();
        $user_id = $request->post('user_id');
        $group_name = $request->post('group_name');
        $description = $request->post('description');        
        $address = $request->post('address');
        $city = $request->post('city');
        $state = $request->post('state');
        $country = $request->post('country');
        $zipCode = $request->post('pin');
        $fbProfile = $request->post('fb_link');
        $lnProfile = $request->post('ln_link');
        $url = $request->post('url');
        $about = $request->post('about');
        $product_list = $request->post('product_list');

        if (isset($_FILES['image']['name'])) {
            $image_name = explode(".", $_FILES['image']['name']);
            $image_name = ($helper->guid4() . '.' . end($image_name));
            $tmp_name = $_FILES['image']['tmp_name'];
        } else {
            $image_name = null;
        }
        
        
        $valid_data = array("group name" => $group_name,    "description" => $description,
                            "address" => $address,          "city" => $city,
                            "state" => $state,              "country" => $country,
                            "zip code" => $zipCode,
                        );

        $validate = $helper->validation($valid_data);
        if (empty($validate)) {
            try {
                $serviceProviderController = new ServiceProviderController();
                $update = $serviceProviderController->updateService($request,$image_name);
                
                $deleted_id = $request->post('deleted_id');
                if($deleted_id){
                    $delete_list = $serviceProviderController->deleteProducts($request);
                }
                if ($update) {                    
                    if (isset($tmp_name)) {
                        foreach (glob("uploads/ServiceProvider/" . $user_id."/*.*") as $filename) {
                            if (is_file($filename)) {
                                unlink($filename);
                            }
                        }
                                                
                        //Create folders in uploads & Save image in folder
                        $path = "uploads/ServiceProvider/$user_id";
                        saveFiles($path, $image_name, $tmp_name);
                    }

                    $user = getUser($user_id, ServiceProvider::USER_TYPE);
                    $user = $user->getJsonData();

                    echo json_encode(array("status" => true, "user" => $user, "message" => "Profile updated successfully"));
                } else {
                    echo json_encode(array("status" => false, "message" => "Profile not updated"));
                }
            } catch (PDOException $e) {
                echo json_encode(array("status" => false, "message" => $e->getMessage()));
            }
        } else {
            echo json_encode(array("status" => false, "message" => "Provide " . implode(",", $validate)));
        }
    } else {
        echo json_encode(array("status" => false, "message" => "You are not authentic user. Please login!"));
    }
}//end updateServiceProviderprofile()

function updateStartupProfile() {

    $app = \Slim\Slim::getInstance();
    $request = $app->request();
    //Enable debugging (on by default)
    $app->config('debug', true);
    $token = $request->headers->get('Auth-Type');    
    $helper = new Helper();
    $date = date('Y-m-d H:i:s');
    $image_name = Null;
    $file_name = Null;
    if (isUserAuthentic($token)) {        
        $user_id = $request->post('user_id');

        if (isset($_FILES['image']['name'])) {
            $image_name = explode(".", $_FILES['image']['name']);
            $image_name = ($helper->guid4() . '.' . end($image_name));
            $tmp_name = $_FILES['image']['tmp_name'];
        }
        
       //echo json_encode(array("status"=>false,"message"=>$request->post('google_link'),$request->post('registered_as'),$request->post('sector'),$request->post('short_description'),$request->post('detailed_description')));
        $valid_data = array("startup name" => $request->post('startup_name'),    "address" => $request->post('address'),
                            "city" => $request->post('city'),                    "state" => $request->post('state'),
                            "country" => $request->post('country'),              "zip code" => $request->post('pin'),
                        );
        $validate = $helper->validation($valid_data);
        if (empty($validate)) {
            try {
                //echo json_encode(array('status'=>false,'message'=>$_FILES['user_file']['name']));die();
                if(isset($_FILES['user_file']['name'])){
                    $count = count($_FILES['user_file']['name']);                        
                    $file_Name = array();
                    for($i=0; $i<$count; $i++ ){
                        $file_Name[$i]= ($helper->guid4().'^'.$_FILES['user_file']['name'][$i]);                            
                    }
                    if(is_array($file_Name)){
                    $file_name = implode(",",$file_Name);
                    }                   
                } 
                //echo json_encode(array('status'=>false,'message'=>$file_name));die();
                $startupController = new StartupController();
                $update =  $startupController->updateStartupFounder($request,$image_name,$file_name);

                if ($update == true) {
                    if($image_name != Null){
                        $profile_image = $request->post('profile_image');
                        unlink("uploads/StartupFounder/" . $user_id."/".$profile_image);
                        
                        //Create folders in uploads & Save image in folder
                        $path = "uploads/StartupFounder/" . $user_id;
                        saveFiles($path, $image_name, $tmp_name); 
                    }
                    if($file_name!=Null){
                        
                        $count = count($_FILES['user_file']['name']);
                        for($i=0;$i<$count;$i++){                                
                            $file_tmp_name = $_FILES['user_file']['tmp_name'][$i];
                            //Create Folders and move image in folder
                            $path = "uploads/StartupFounder/$user_id";
                            saveFiles($path, $file_Name[$i], $file_tmp_name);
                        }
                    }
                    
                    $delete_file = $request->post('deleted_fileName_list');
                    $delete_file = json_decode($delete_file, true);
                    //echo json_encode(array('status'=>false,'message'=>$delete_file));
                    if($delete_file){
                        foreach ($delete_file as $del){
                           unlink("uploads/StartupFounder/" . $user_id."/".$del);                            
                        }
                    }
                    
                      

                    $user = getUser($user_id, StartUp::USER_TYPE);
                    $user = $user->getJsonData();
                    //echo json_encode(array("status" => FALSE,"message" => $user));die();
                    echo json_encode(array("status" => true, "user" => $user, "message" => "Profile updated successfully"));
                } else {
                    echo json_encode(array("status" => false, "message" => $result));
                }
            } catch (PDOException $e) {
                echo json_encode(array("status" => false, "message" => $e->getMessage()));
            }
        } else {
            echo json_encode(array("status" => false, "message" => "Provide " . implode(",", $validate)));
        }
    } else {
        echo json_encode(array("status" => false, "message" => "You are not authentic user. Please login!"));
    }
}

function userPostUpdates() {

    $request = \Slim\Slim::getInstance()->request();
    $user_id = $request->post('user_id');
    $email = $request->post('email');

    $message = $request->post('message');
    $mType = $request->post('media_type');
    $mPath = " ";

    if (isUserExists($email)) {

        $sql = "INSERT INTO sc_updates (user_id,message, media_type, media_path,created_date,updated_date) VALUES 	(:sc_user_id ,:sc_message, :sc_mtype, :sc_mpath,:sc_created_date,:sc_updated_date)";
        try {
            $db = getDB();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("sc_user_id", $user_id);
            $stmt->bindParam("sc_message", $message);
            $stmt->bindParam("sc_mtype", $mType);
            $stmt->bindParam("sc_mpath", $mPath);

            $date = date("Y-m-d H:m:s");

            $stmt->bindParam("sc_created_date", $date);
            $stmt->bindParam("sc_updated_date", $date);
            $stmt->execute();
            $user_id = $db->lastInsertId();
            $db = null;

            echo json_encode(array("status" => true, "user_id" => $user_id, "message" => "Post updated"));
        } catch (PDOException $e) {
            //error_log($e->getMessage(), 3, '/var/tmp/php.log');
            echo jsone_encode(array("status" => false, "message" => $e->getMessage()));
        }
    } else {
        echo json_encode(array("status" => false, "message" => "Invalid request"));
    }
}

function getAllUpdates() {

    $request = \Slim\Slim::getInstance()->request();
    $user_id = $request->post('user_id');
    $email = $request->post('email');

    if (isUserExists($email)) {

        $sql = "SELECT * from sf_updates where 
		user_id = :user_id ORDER BY created_date desc";

        try {
            $db = getDB();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("user_id", $user_id);
            $stmt->execute();
            $notes = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            echo jsone_encode(array("status" => true, "updates" => $notes, "message" => null));
        } catch (PDOException $e) {
            //error_log($e->getMessage(), 3, '/var/tmp/php.log');
            echo jsone_encode(array("status" => false, "message" => $e->getMessage()));
        }
    } else {
        echo '{"status":false,"message":"Invalid request"}';
    }
}

// Create folder acording to path & save files like images and other in folders
function saveFiles($path, $image_name, $tmp_name) {
    
    try {

        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }
        move_uploaded_file($tmp_name, $path . "/$image_name");
    } catch (PDOException $e) {
        $e->getMessage();
    }
}

//Register Angel individual Profile
function registerAngelIndividualProfile() {

    $request = \Slim\Slim::getInstance()->request();
    $user_id = $request->post('user_id');
    $name = $request->post('name');
    $add = $request->post('address');
    $city = $request->post('city');
    $state = $request->post('state');
    $country = $request->post('country');
    $pin = $request->post('pin');
    $fb_link = $request->post('fb_link');
    $ln_link = $request->post('ln_link');
    $tw_link = $request->post('tw_link');
    $blog_link = $request->post('blog_link');
    $invfrom = $request->post('investing_from');
    $type_budget_amount = $request->post('type_budget_amount');
    $invbudget = $request->post('inv_budget_per_year');
    $type_cap_amount = $request->post('type_cap_amount');
    $invcap = $request->post('inv_cap_per_startup');
    $interest_areas = $request->post('interest_areas');
    $about = $request->post('about');
    $work_exp = $request->post('work_exp');
    $education = $request->post('education');
    


    $valid_value = array(   "name" => $name,                              "address" => $add,
                            "city" => $city,                              "state" => $state,
                            "country" => $country,                        "pin" => $pin,
                            "investing form" => $invfrom,                 "type of budget amount" => $type_budget_amount,
                            "investment budget per year" => $invbudget,   "type of cap amount" => $type_cap_amount,
                            "investment cap per startup" => $invcap,      "interest areas" => $interest_areas,
                            "about" => $about,                            "work experience" => $work_exp,
                            "education" => $education
                        );

    if (isUserExists(null, $user_id)){
        if(isUserRegister($user_id,IndividualInvestor::USER_TYPE )){

            $helper = new Helper();
            $validate = $helper->validation($valid_value);

            if (empty($validate)) {
                try {
                    $individualInvestorController = new IndividualInvestorController();
                    $investorController = $individualInvestorController->registerIndividualInvestor($request);

                    if ($investorController) {
                        //Update usertype value in sc_user tabe
                        updateUserTypeAndProfileStatus($user_id, IndividualInvestor::USER_TYPE);

                        $user = getUser($user_id, IndividualInvestor::USER_TYPE);
                        $user = $user->getJsonData();

                        echo json_encode(array("status" => true, "user" => $user, "user_type" => "Angel Individual", "message" => "Register Angel Individual Profile"));
                    }
                } catch (PDOException $e) {
                    echo json_encode(array("status" => false, "message" => $e->getMessage()));
                }
            } else {
                echo json_encode(array("status" => false, "message" => "Provide " . implode(",", $validate)));
            }
        }else{
            echo json_encode(array("status" => false, "message" => "User already exists"));
        }
    } else {
        echo json_encode(array("status" => false, "message" => "Invalid user"));
    }
}// end registerAngelIndividualProfile()

// Angel Group investor Profile
function registerAngelGroupProfile() {
    $request = \Slim\Slim::getInstance()->request();
    $helper = new Helper();
    $image_name = "";
    
    $user_id = $request->post('user_id');
    $group_name = $request->post('group_name');
    $description = $request->post('description');
    $address = $request->post('address');
    $city = $request->post('city');
    $state = $request->post('state');
    $country = $request->post('country');
    $pin = $request->post('pin');
    $fb_link = $request->post('fb_link');
    $ln_link = $request->post('ln_link');
    $url = $request->post('url');
    $type_total_fund = $request->post('type_total_fund');
    $total_fund = $request->post('total_fund');
    $type_investment = $request->post('type_investment');
    $type_investment_size = $request->post('type_investment_size');
    $investment_size = $request->post('investment_size');
    $other_criteria = $request->post('other_criteria');
    $investment_areas = $request->post('investment_areas');
    

    $valid_data = array("group name" => $group_name, "description" => $description,
        "address" => $address, "city" => $city,
        "state" => $state, "country" => $country,
        "zip code" => $pin,
    );
    
    if (isset($_FILES['image'])) {
        $tmp_name = $_FILES['image']['tmp_name'];
        $image_name = explode(".", $_FILES['image']['name']);
        $image_name = ($helper->guid4() . '.' . end($image_name));

        //Create folders in uploads & Save image in folder
        $path = "uploads/AngelGroupInvestment/$user_id";
        saveFiles($path, $image_name, $tmp_name);
    }

    if (isUserExists(null, $user_id)) {
        if(isUserRegister($user_id, GroupInvestor::USER_TYPE)){
            $validate = $helper->validation($valid_data);
            if (empty($validate)) {
                
                $groupInvestorController = new GroupInvestorController();
                $register = $groupInvestorController->registeGroupInvestor($request,$image_name);

                try {

                    if($register){
                        
                        

                        //Update usertype value in sc_user tabe
                        updateUserTypeAndProfileStatus($user_id, GroupInvestor::USER_TYPE);

                        $user = getUser($user_id, GroupInvestor::USER_TYPE);
                        $user = $user->getJsonData();
                    
                        echo json_encode(array("status" => true, "user" => $user, "user_type" => "Angel Group", "message" => "Register Angel Group Profile"));
                    }
                } catch (PDOException $e) {
                    echo json_encode(array("status" => false, "message" => $e->getMessage()));
                }
            } else {
                echo json_encode(array("status" => false, "message" => "Provide " . implode(",", $validate)));
            }
        }else{
            echo json_encode(array("status" => false, "message" => "User already exists"));
        }
    }else{
            echo json_encode(array("status" => false, "message" => "Invalid user"));
    }
}//end registerAngelGroupProfile()

function addUserInterestAreas($user_id, $areas) {

    if (isUserExists($email)) {

        $sql = "INSERT INTO interest_areas (user_id,sector_id,created_date,updated_date) VALUES (:sc_user_id ,:sc_sector_id,:sc_created_date,:sc_updated_date)";

        try {
            $db = getDB();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("sc_user_id", $user_id);
            $stmt->bindParam("sc_sector_id", $about);

            $date = date("Y-m-d H:m:s");

            $stmt->bindParam("sc_created_date", $date);
            $stmt->bindParam("sc_updated_date", $date);
            $stmt->execute();
            $id = $db->lastInsertId();
            $db = null;

            echo json_encode(array("status" => true, "id" => $id, "message" => "User intrested area added"));
        } catch (PDOException $e) {
            //error_log($e->getMessage(), 3, '/var/tmp/php.log');
            echo json_encode(array("status" => false, "message" => $e->getMessage()));
        }
    } else {
        echo json_encode(array("status" => false, "message" => "Invalid request"));
    }
}

function registerServiceProvider() {

    $request = \Slim\Slim::getInstance()->request();
    $helper = new Helper();

    $user_id = $request->post('user_id');
    $group_name = $request->post('group_name');
    $description = $request->post('description');
    $address = $request->post('address');
    $city = $request->post('city');
    $state = $request->post('state');
    $country = $request->post('country');
    $zipCode = $request->post('pin');
    $fbProfile = $request->post('fb_link');
    $lnProfile = $request->post('ln_link');
    $url = $request->post('url');
    $about = $request->post('about');
   
     if (isset($_FILES['image'])) {
                        $image_name = explode(".", $_FILES['image']['name']);
                        $image_name = ($helper->guid4() . '.' . end($image_name));
                        $tmp_name = $_FILES['image']['tmp_name'];
     }else{
         $image_name = null;
     }

    $valid_data = array(    "group name" => $group_name,    "description" => $description,
                            "address" => $address,          "city" => $city,
                            "state" => $state,              "country" => $country,
                            "zip code" => $zipCode,
                        );
    
    if (isUserExists(null, $user_id)) {
        if(isUserRegister($user_id, ServiceProvider::USER_TYPE)){
            $validate = $helper->validation($valid_data);
            if (empty($validate)) {
                 $serviceProviderController = new ServiceProviderController();
                 $register = $serviceProviderController->registerService($request,$image_name);
                if($register){
                    if (isset($tmp_name)) {                        
                        // Set Path of image & call saveFiles function
                        $path = "uploads/ServiceProvider/$user_id";
                        saveFiles($path, $image_name, $tmp_name);
                    }

                    //Update user type in sc_users table
                    updateUserTypeAndProfileStatus($user_id, ServiceProvider::USER_TYPE);
                    $user = getUser($user_id, ServiceProvider::USER_TYPE);
                    $user = $user->getJsonData();
                    echo json_encode(array("status" => true, "user" => $user, "user_type" => "Service Provider", "message" => "Service provider register"));
                } else {
                    echo json_encode(array("status" => false, "message" => 'User not registered'));
                }
            } else {
            echo json_encode(array("status" => false, "message" => "Provide " . implode(",", $validate)));
            }
        } else {
            echo json_encode(array("status" => false, "message" => "User already exists"));
        }    
    } else {
        echo json_encode(array("status" => false, "message" => "Invalid user"));
    }
}

// Registration for Startup Founders
function registerStartupFounder() {

    $request = \Slim\Slim::getInstance()->request();
    $helper = new Helper();

    $user_id = $request->post('user_id');
    $startup_name = $request->post('startup_name');    
    $address = $request->post('address');
    $city = $request->post('city');
    $state = $request->post('state');
    $country = $request->post('country');
    $pin = $request->post('pin');
    $registered_as = $request->post('registered_as');
    //echo json_encode(array('status'=>false,'message'=>$registered_as));die();
    
     $valid_data = array(   "startup name" => $startup_name,    "address" => $address,
                            "city" => $city,                    "state" => $state,
                            "country" => $country,              "zip code" => $pin,
                        );

    if (isUserExists(Null, $user_id)) {
        $image_name = Null;
        $file_name = Null;
        if (isUserRegister($user_id, StartUp::USER_TYPE)) {
            $validate = $helper->validation($valid_data);
            if (empty($validate)) {
                try {
                    $helper = new Helper();                    
                    $id = $helper->guid4();
                    $date = date('Y-m-d H:i:s');
                    
                    if (isset($_FILES['image']['name'])) {
                        $image_name = explode(".", $_FILES['image']['name']);
                        $image_name = ($helper->guid4() . '.' . end($image_name));
                        $tmp_name = $_FILES['image']['tmp_name'];
                    }
                   //echo json_encode(array('status'=>false,'message'=>$_FILES['user_file']));die();
                    if(isset($_FILES['user_file']['name'])){
                        $count = count($_FILES['user_file']['name']);                        
                        $file_Name = array();
                        for($i=0; $i<$count; $i++ ){
                            $file_Name[$i]= ($helper->guid4().'^'.$_FILES['user_file']['name'][$i]);                            
                        }
                        $file_name = implode(",",$file_Name);
                    }
                                
                    $startupController = new StartupController();
                    $register =  $startupController->registerStartup($request,$image_name,$file_name);
                    
                    if($register){
                        if($image_name!=Null){
                            //Create Folders and move image in folder
                            $path = "uploads/StartupFounder/$user_id";
                            saveFiles($path, $image_name, $tmp_name);
                        }
                        if($file_name!=Null){
                            $count = count($_FILES['user_file']['name']);
                            for($i=0;$i<$count;$i++){                                
                                $file_tmp_name = $_FILES['user_file']['tmp_name'][$i];
                                //Create Folders and move image in folder
                                $path = "uploads/StartupFounder/$user_id";
                                saveFiles($path, $file_Name[$i], $file_tmp_name);
                            }
                        }

                        //Update user type in sc_users table
                        updateUserTypeAndProfileStatus($user_id, StartUp::USER_TYPE);
                        $user = getUser($user_id, StartUp::USER_TYPE);
                        $user = $user->getJsonData();
                        //echo json_encode(array('status'=>false,'message'=>$user));die();
                        echo json_encode(array("status" => true, "user" => $user, "user_type" => "Startup Founder", "message" => "Startup Founder register"));
                    }else{
                        echo json_encode(array("status" => false, "message" => 'Registration failed'));
                    }    
                } catch (PDOException $exc) {
                    echo json_encode(array("status" => false, "message" => $exc->getMessage()));
                }
            } else {
                echo json_encode(array("status" => false, "message" => "Provide " . implode(",", $validate)));
            }
        } else {
            echo json_encode(array("status" => false, "message" => "User already exists"));
        }
    } else {
        echo json_encode(array("status" => false, "message" => "Invalid user"));
    }
}

//Provide profile Image at update profile time
function getProfileImage() {
    $request = \Slim\Slim::getInstance()->request();

    $user_id = $request->post('user_id');
    $image_name = $request->post('image_name');
    $user_type = $request->post('user_type');
    
    $valid_data = array("user id"=>$user_id, "image name"=>$image_name, "user type"=>$user_type);

    //issue regarding path, converting provided user type value to respective string values
    $user_type = ($user_type == StartUp::USER_TYPE ? "StartupFounder" : ($user_type == GroupInvestor::USER_TYPE ? "AngelGroupInvestment" : ($user_type == ServiceProvider::USER_TYPE ? "ServiceProvider" : "")));
    $helper = new Helper();
    $validate = $helper->validation($valid_data);
    if (empty($validate)) {
        try {
            $image_file = file_get_contents("uploads/$user_type/$user_id/$image_name");
            $photo = base64_encode($image_file);
            if ($photo) {
                echo json_encode(array("status" => true, "image_name" => $image_name, "profile_image" => $photo, "message" => "Profile Image found"));
            } else {
                echo json_encode(array("status" => false, "message" => "Profile Image not found"));
            }
        } catch (PDOException $e) {
            $e->getMessage();
        }
    } else {
        echo json_encode(array("status" => false, "message" => "Provide " . implode(",", $validate)));
    }
}//end getProfileImage()

function inviteUsers() {

    $request = \Slim\Slim::getInstance()->request();

    $user_id = $request->post('user_id');
    $email = $request->post('email');

    $invited_user_id = $request->post('invited_user_id');

    $first_name = $request->post('first_name');
    $last_name = $request->post('last_name');
    $email = $request->post('email');
    $mobile_no = $request->post('mobile_no');
    $invite_type = $request->post('invite_type');

    $status = 1;

    if (isUserExists($email)) {

        $sql = "INSERT INTO sc_service_provider (invited_user_id,invited_by_user_id, first_name, last_name,email,mobile_no,invite_type,status,created_date,updated_date) VALUES 	(:sc_invited_user_id,:sc_invited_by_user_id,:sc_first_name,:sc_last_name,:sc_email,:sc_mobile_no,:sc_invite_type,:sc_status,:sc_created_date,:sc_updated_date)";
        try {
            $db = getDB();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("sc_invited_user_id", $invited_user_id);
            $stmt->bindParam("sc_invited_by_user_id", $user_id);
            $stmt->bindParam("sc_first_name", $first_name);
            $stmt->bindParam("sc_last_name", $last_name);
            $stmt->bindParam("sc_email", $email);
            $stmt->bindParam("sc_mobile_no", $mobile_no);
            $stmt->bindParam("sc_invite_type", $invite_type);
            $stmt->bindParam("sc_status", $status);

            $date = date("Y-m-d H:m:s");

            $stmt->bindParam("sc_created_date", $date);
            $stmt->bindParam("sc_updated_date", $date);
            $stmt->execute();
            $invite_id = $db->lastInsertId();
            $db = null;

            echo jsone_encode(array("status" => true, "id" => $user_id, "message" => "User invite sent"));
        } catch (PDOException $e) {
            //error_log($e->getMessage(), 3, '/var/tmp/php.log');
            echo json_encode(array("status" => false, "message" => $e->getMessage()));
        }
    } else {

        echo json_encode(array("status" => false, "message" => "Invalid request"));
    }
}

function addServiceProviderProduct() {

    $request = \Slim\Slim::getInstance()->request();

    $user_id = $request->post('user_id');
    $email = $request->post('email');

    $service_provider_id = $request->post('service_provider_id');
    $pname = $request->post('p_name');
    $pdesc = $request->post('p_description');
    $pcategory = $request->post('p_category');


    $status = 1;

    if (isUserExists($email)) {

        $sql = "INSERT INTO sc_service_provider_products (service_provider_id,name, category, description,created_date,updated_date) VALUES 	(:sc_service_provider_id,:sc_name,:sc_category,:sc_description,:sc_created_date,:sc_updated_date)";
        try {
            $db = getDB();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("sc_service_provider_id", $service_provider_id);
            $stmt->bindParam("sc_name", $pname);
            $stmt->bindParam("sc_category", $pdesc);
            $stmt->bindParam("sc_description", $pcategory);


            $date = date("Y-m-d H:m:s");

            $stmt->bindParam("sc_created_date", $date);
            $stmt->bindParam("sc_updated_date", $date);
            $stmt->execute();
            $product_id = $db->lastInsertId();
            $db = null;

            echo json_encode(array("status" => true, "id" => $product_id, "message" => "Service provider product added"));
        } catch (PDOException $e) {
            //error_log($e->getMessage(), 3, '/var/tmp/php.log');
            echo json_encode(array("status" => false, "message" => $e->getMessage()));
        }
    } else {

        echo json_encode(array("status" => false, "message" => "Invalid request"));
    }
}

//Provide user information
function getUser($userId, $userType) {
    switch ($userType) {
        case "0":
            return array("user_id" => $userId);
        case StartUp::USER_TYPE :
            $startupController = new StartupController();
            return $startupController->getByUserId($userId);

        case IndividualInvestor::USER_TYPE :
            $individualInvestorController = new IndividualInvestorController();
            return $individualInvestorController->getByUserId($userId);

        case GroupInvestor::USER_TYPE :
            $groupInvestorController = new GroupInvestorController();
            return $groupInvestorController->getByUserId($userId);

        case ServiceProvider::USER_TYPE :
            $serviceProviderController = new ServiceProviderController();
            return $serviceProviderController->getByUserId($userId);
    }
}

//Check User is registered in Profile tables
function isUserRegister($userId, $userType) {
    switch ($userType) {
        case StartUp::USER_TYPE :
            $startupController = new StartupController();
            return $startupController->userExists($userId);
         
        case IndividualInvestor::USER_TYPE :
            $individualInvestorController = new IndividualInvestorController();
            return $individualInvestorController->userExists($userId);
            
        case GroupInvestor::USER_TYPE :
            $groupInvestorController = new GroupInvestorController();
            return $groupInvestorController->userExists($userId);
            
        case ServiceProvider::USER_TYPE :
            $serviceProviderController = new ServiceProviderController();
            return $serviceProviderController->userExists($userId);    
    }
}

// below this was commented
/*
  function deleteNote() {

  $request = \Slim\Slim::getInstance()->request();
  $user_id = $request->post('user_id');
  $note_id = $request->post('note_id');

  if(isUserActive($user_id)){

  $sql = "UPDATE sf_notes SET deleted= 1 , updated_date=:sf_updated_date where id = :note_id and user_id = :user_id";
  try {
  $db = getDB();
  $stmt = $db->prepare($sql);
  $stmt->bindParam("note_id", $note_id);
  $stmt->bindParam("user_id", $user_id);
  $time=time();
  $stmt->bindParam("sf_updated_date", $time);
  $stmt->execute();
  $db = null;
  echo '{"status":true}';
  } catch(PDOException $e) {
  echo '{"status":false,"error":'. $e->getMessage() .'}';
  }
  }else{
  echo '{"status":false,"error":"Your not authorized to get data"}';
  }

  }


  function insertUser() {
  $request = \Slim\Slim::getInstance()->request();
  $social_id = $request->post('social_id');
  $user_name = $request->post('user_name');
  $user_email = $request->post('user_email');
  $sf_aes = $request->post('sf_aes');
  $sf_key = $request->post('sf_key');

  if(isUserExists($user_email)){

  getUserInfo($user_email);

  }else{

  $sql = "INSERT INTO sf_users (user_social_id,user_name, user_email,sf_aes,sf_key,created_date,updated_date) 	VALUES (:sf_social_id ,:sf_name , :sf_email,:sf_aes,:sf_key,:sf_created_date,:sf_updated_date)";

  try {

  $db = getDB();
  $stmt = $db->prepare($sql);
  $stmt->bindParam("sf_social_id", $social_id);
  $stmt->bindParam("sf_name", $user_name);
  $stmt->bindParam("sf_email", $user_email);
  $stmt->bindParam("sf_aes", $sf_aes);
  $stmt->bindParam("sf_key", $sf_key);
  $time=time();
  $stmt->bindParam("sf_created_date", $time);
  $stmt->bindParam("sf_updated_date", $time);
  $stmt->execute();
  $user_id = $db->lastInsertId();
  $db = null;

  getUserInfo($user_email);
  } catch(PDOException $e) {
  //error_log($e->getMessage(), 3, '/var/tmp/php.log');
  echo '{"status":false,"error":'. $e->getMessage() .'}';
  }
  }


  }


  function getUserInfo($email) {

  $sql = "SELECT * FROM sf_users WHERE user_email = :sf_email";
  try {
  $db = getDB();
  $stmt = $db->prepare($sql);
  $stmt->bindParam("sf_email", $email);
  $stmt->execute();
  $user= $stmt->fetchAll(PDO::FETCH_OBJ);
  $db = null;
  echo '{"status":true,"user": ' . json_encode($user) . '}';
  } catch(PDOException $e) {
  echo '{"status":false,"error":'. $e->getMessage() .'}';
  }
  }

  function isUserExists($email) {
  try {
  $db = getDB();
  $stmt = $db->prepare("SELECT user_id from sf_users WHERE user_email = :sf_email");
  $stmt->bindParam("sf_email", $email);
  $stmt->execute();
  $rows = $stmt->fetchAll();
  $db = null;
  $num_rows = count($rows);;
  return $num_rows>0;
  } catch(PDOException $e) {

  return false ;
  }
  }

  function isUserActive($email) {
  try {
  $db = getDB();
  $stmt = $db->prepare("SELECT user_id from sf_users WHERE user_email = :sf_email and active=1");
  $stmt->bindParam("sf_email", $email);
  $stmt->execute();
  $rows = $stmt->fetchAll();
  $db = null;
  $num_rows = count($rows);;
  return $num_rows>0;
  } catch(PDOException $e) {

  return false ;
  }
  }

  function updateKey() {

  $request = \Slim\Slim::getInstance()->request();
  $user_id = $request->post('user_id');
  $user_key = $request->post('user_key');

  if(isUserActive($user_id)){
  $sql = "UPDATE sf_users SET sf_aes=:aes_key , updated_date=:sf_updated_date where user_email = :sf_email";
  try {
  $db = getDB();
  $stmt = $db->prepare($sql);
  $stmt->bindParam("sf_email", $user_id);
  $stmt->bindParam("aes_key", $user_key);
  $time=time();
  $stmt->bindParam("sf_updated_date", $time);
  $stmt->execute();
  $db = null;
  echo '{"status":true}';
  } catch(PDOException $e) {
  //error_log($e->getMessage(), 3, '/var/tmp/php.log');
  echo '{"status":false,"error":'. $e->getMessage() .'}';
  }
  }else{
  echo '{"status":false,"error":"Your not authorized to get data"}';
  }


  }


  function random_password( $length = 8 ) {
  $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
  //$password = substr( str_shuffle( $chars ), 0, $length );
  return $password;
  } */
//?>