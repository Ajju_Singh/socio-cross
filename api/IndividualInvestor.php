<?php

//namespace DataBase;
require_once 'db.php';

/**
 * 
 * @author Admin
 */
class IndividualInvestor {

    const USER_TYPE = "2.1";

    private $id;
    private $user_id;
    private $user_type;
    private $name;
    private $address;
    private $city;
    private $state;
    private $country;
    private $pin;
    private $fb_link;
    private $ln_link;
    private $tw_link;
    private $blog_link;
    private $investing_from;
    private $type_budget_amount;
    private $inv_budget_per_year;
    private $type_cap_amount;
    private $inv_cap_per_startup;
    private $interest_areas;
    private $about;
    private $work_exp;
    private $education;
    private $individualHistoryList = array();

    function __construct($individualInvestor = array()) {
        $this->id = isset($individualInvestor['id']) ? $individualInvestor['id'] : null;
        $this->user_id = isset($individualInvestor['user_id']) ? $individualInvestor['user_id'] : null;
        $this->user_type = isset($individualInvestor['user_type']) ? $individualInvestor['user_type'] : null;
        $this->name = isset($individualInvestor['name']) ? $individualInvestor['name'] : null;
        $this->address = isset($individualInvestor['address']) ? $individualInvestor['address'] : null;
        $this->city = isset($individualInvestor['city']) ? $individualInvestor['city'] : null;
        $this->state = isset($individualInvestor['state']) ? $individualInvestor['state'] : null;
        $this->country = isset($individualInvestor['country']) ? $individualInvestor['country'] : null;
        $this->pin = isset($individualInvestor['zip_code']) ? $individualInvestor['zip_code'] : null;
        $this->fb_link = isset($individualInvestor['fb_profile']) ? $individualInvestor['fb_profile'] : null;
        $this->ln_link = isset($individualInvestor['ln_profile']) ? $individualInvestor['ln_profile'] : null;
        $this->tw_link = isset($individualInvestor['tw_profile']) ? $individualInvestor['tw_profile'] : null;
        $this->blog_link = isset($individualInvestor['blog']) ? $individualInvestor['blog'] : null;
        $this->investing_from = isset($individualInvestor['investing_from']) ? $individualInvestor['investing_from'] : null;
        $this->type_budget_amount = isset($individualInvestor['type_budget_amount']) ? $individualInvestor['type_budget_amount'] : null;
        $this->inv_budget_per_year = isset($individualInvestor['investment_budget_per_year']) ? $individualInvestor['investment_budget_per_year'] : null;
        $this->type_cap_amount = isset($individualInvestor['type_cap_amount']) ? $individualInvestor['type_cap_amount'] : null;
        $this->inv_cap_per_startup = isset($individualInvestor['investment_cap_per_startup']) ? $individualInvestor['investment_cap_per_startup'] : null;
        $this->interest_areas = isset($individualInvestor['interest_areas']) ? $individualInvestor['interest_areas'] : null;
        $this->about = isset($individualInvestor['about_yourself']) ? $individualInvestor['about_yourself'] : null;
        $this->work_exp = isset($individualInvestor['work_experience']) ? $individualInvestor['work_experience'] : null;
        $this->education = isset($individualInvestor['education']) ? $individualInvestor['education'] : null;

        if (isset($individualInvestor['individualHistoryList']) && $individualInvestor['individualHistoryList']) {
            $this->individualHistoryList = IndividualInvestorHistory::history($individualInvestor['individualHistoryList']);
        } else {
            $this->individualHistoryList = null;
        }
    }

    function getJsonData() {
        $var = get_object_vars($this);
        foreach ($var as &$value) {
            if (is_object($value) && method_exists($value, 'getJsonData')) {
                $value = $value->getJsonData();
            }
        }
        return $var;
    }

}

class IndividualInvestorHistory {

    private $startup_id;
    private $startup_name;
    private $startup_amount;

    public static function history($history) {
        $investorHistory = array();
        foreach ($history as $h) {
            $investorHistory[] = array(
                'startup_id' => $h['id'],
                'startup_name' => $h['startup_name'],
                'startup_amount' => $h['amount']
            );
        }
        return $investorHistory;
    }

}

class IndividualInvestorController {

    function getByUserId($userId) {
        $query = "SELECT saip.*, u.user_type FROM sc_angel_individual_profile saip INNER JOIN sc_users u ON u.id = saip.user_id "
                . " WHERE saip.user_id = :sc_user_id";

        $db = getDB();
        $stmt = $db->prepare($query);
        $stmt->bindParam("sc_user_id", $userId);
        $stmt->execute();
        $individualInvestor = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($individualInvestor) {
            $query2 = "SELECT saiih.id, saiih.startup_name, saiih.amount"
                    . " FROM sc_angel_individual_investment_history saiih WHERE angel_individual_id = :angel_individual_id";
            $stmt = $db->prepare($query2);
            $stmt->bindParam("angel_individual_id", $individualInvestor['id']);
            $stmt->execute();
            $individualInvestor['individualHistoryList'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return new IndividualInvestor($individualInvestor);
        }
        return null;
    }
    
    function userExists($userId){
        $query = "SELECT user_id FROM sc_angel_individual_profile "
                . " WHERE user_id = :sc_user_id";

        $db = getDB();
        $stmt = $db->prepare($query);
        $stmt->bindParam("sc_user_id", $userId);
        $stmt->execute();
        $userRegistered = $stmt->fetch(PDO::FETCH_ASSOC);
        if($userRegistered){
            return false;
        }else{
            return true;
        }
    }

    function registerIndividualInvestor($request) {
        $user_id = $request->post('user_id');
        $name = $request->post('name');
        $add = $request->post('address');
        $city = $request->post('city');
        $state = $request->post('state');
        $country = $request->post('country');
        $pin = $request->post('pin');
        $fb_link = $request->post('fb_link');
        $ln_link = $request->post('ln_link');
        $tw_link = $request->post('tw_link');
        $blog_link = $request->post('blog_link');
        $invfrom = $request->post('investing_from');
        $type_budget_amount = $request->post('type_budget_amount');
        $invbudget = $request->post('inv_budget_per_year');
        $type_cap_amount = $request->post('type_cap_amount');
        $invcap = $request->post('inv_cap_per_startup');
        $interest_areas = $request->post('interest_areas');
        $about = $request->post('about');
        $work_exp = $request->post('work_exp');
        $education = $request->post('education');
    
        $sql = "INSERT INTO sc_angel_individual_profile (id,user_id,name,address,city,state,country,zip_code,fb_profile,ln_profile,tw_profile,blog,investing_from,investment_budget_per_year,type_budget_amount,investment_cap_per_startup,type_cap_amount,about_yourself,work_experience,education,interest_areas,created_date) "
                . "VALUES (:sc_id,:sc_user_id,:sc_name,:sc_address,:sc_city,:sc_state,:sc_country,:sc_pin,:sc_fb_link,:sc_ln_link,:sc_tw_link,:sc_blog_link,:sc_invfrom,:sc_invbudget,:sc_type_budget_amount,:sc_invcap,:sc_type_cap_amount,:sc_about,:sc_workexp,:sc_education,:sc_intareas,:sc_created_date)";

        $helper = new Helper();
        $id = $helper->guid4();
        $date = date('Y-m-d H:i:s');
        $db = getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("sc_id", $id);
        $stmt->bindParam("sc_user_id", $user_id);
        $stmt->bindParam("sc_name", $name);
        $stmt->bindParam("sc_address", $add);
        $stmt->bindParam("sc_city", $city);
        $stmt->bindParam("sc_state", $state);
        $stmt->bindParam("sc_country", $country);
        $stmt->bindParam("sc_pin", $pin);
        $stmt->bindParam("sc_fb_link", $fb_link);
        $stmt->bindParam("sc_ln_link", $ln_link);
        $stmt->bindParam("sc_tw_link", $tw_link);
        $stmt->bindParam("sc_blog_link", $blog_link);
        $stmt->bindParam("sc_invfrom", $invfrom);
        $stmt->bindParam("sc_invbudget", $invbudget);
        $stmt->bindParam("sc_type_budget_amount", $type_budget_amount);
        $stmt->bindParam("sc_invcap", $invcap);
        $stmt->bindParam("sc_type_cap_amount", $type_cap_amount);
        $stmt->bindParam("sc_about", $about);
        $stmt->bindParam("sc_workexp", $work_exp);
        $stmt->bindParam("sc_education", $education);
        $stmt->bindParam("sc_intareas", $interest_areas);
        $stmt->bindParam("sc_created_date", $date);
        $stmt->execute();

        $history = $request->post('individualHistoryList');

        if (!empty($history)) {
            $history = json_decode($history, true);

            if (!empty($history[0]['startup_name'])) {
                $sql = 'INSERT INTO sc_angel_individual_investment_history (id,angel_individual_id,startup_name,amount,created_date) VALUES ';
                $insertQuery = array();
                $insertData = array();

                foreach ($history as $i => $h) {
                    $insertQuery[] = '(:id' . $i . ', :angel_individual_id' . $i . ', :startup_name' . $i . ', :amount' . $i . ', :created_date' . $i . ')';
                    $insertData['id' . $i]                  = $helper->guid4();
                    $insertData['angel_individual_id' . $i] = $id;
                    $insertData['startup_name' . $i]        = $h['startup_name'];
                    $insertData['amount' . $i]              = $h['startup_amount'];
                    $insertData['created_date' . $i]        = date('Y-m-d H:i:s');
                }

                if (!empty($insertQuery)) {
                    $sql .= implode(', ', $insertQuery);
                    $stmt = $db->prepare($sql);
                    $stmt->execute($insertData);
                }
            }
        }
        return true;
    }
    
    function updateIndividualInvestor($request){
        $id                 = $request->post('id');
        $user_id            = $request->post('user_id');
        $name               = $request->post('name');
        $add                = $request->post('address');
        $city               = $request->post('city');
        $state              = $request->post('state');
        $country            = $request->post('country');
        $pin                = $request->post('pin');
        $fb_link            = $request->post('fb_link');
        $ln_link            = $request->post('ln_link');
        $tw_link            = $request->post('tw_link');
        $blog_link          = $request->post('blog_link');
        $invfrom            = $request->post('investing_from');
        $type_budget_amount = $request->post('type_budget_amount');
        $invbudget          = $request->post('inv_budget_per_year');
        $type_cap_amount    = $request->post('type_cap_amount');
        $invcap             = $request->post('inv_cap_per_startup');
        $interest_areas     = $request->post('interest_areas');
        $about              = $request->post('about');
        $work_exp           = $request->post('work_exp');
        $education          = $request->post('education');
        
        try{
            $query = "UPDATE sc_angel_individual_profile SET name = :sc_name, address = :sc_address, city = :sc_city, state = :sc_state, country = :sc_country, zip_code = :sc_pin, fb_profile = :sc_fb_link, ln_profile = :sc_ln_link, tw_profile = :sc_tw_link, blog = :sc_blog_link, investing_from = :sc_invfrom, type_budget_amount = :sc_type_budget_amount,"
                        . "investment_budget_per_year = :sc_invbudget, type_cap_amount = :sc_type_cap_amount, investment_cap_per_startup = :sc_invcap, interest_areas = :sc_intareas, about_yourself = :sc_about, work_experience = :sc_workexp, education = :sc_education, updated_date = :sc_update_date"  
                        . " WHERE user_id = :sc_user_id";

            $db = getDB();
            $date = date('Y-m-d H:i:s');
            $stmt = $db->prepare($query);
            $stmt->bindParam("sc_user_id", $user_id);
            $stmt->bindParam("sc_name", $name);
            $stmt->bindParam("sc_address", $add);
            $stmt->bindParam("sc_city", $city);
            $stmt->bindParam("sc_state", $state);
            $stmt->bindParam("sc_country", $country);
            $stmt->bindParam("sc_pin", $pin);
            $stmt->bindParam("sc_fb_link", $fb_link);
            $stmt->bindParam("sc_ln_link", $ln_link);
            $stmt->bindParam("sc_tw_link", $tw_link);
            $stmt->bindParam("sc_blog_link", $blog_link);
            $stmt->bindParam("sc_invfrom", $invfrom);
            $stmt->bindParam("sc_invbudget", $invbudget);
            $stmt->bindParam("sc_type_budget_amount", $type_budget_amount);
            $stmt->bindParam("sc_invcap", $invcap);
            $stmt->bindParam("sc_type_cap_amount", $type_cap_amount);
            $stmt->bindParam("sc_about", $about);
            $stmt->bindParam("sc_workexp", $work_exp);
            $stmt->bindParam("sc_education", $education);
            $stmt->bindParam("sc_intareas", $interest_areas);
            $stmt->bindParam("sc_update_date", $date);
            $stmt->execute();


                $history = $request->post('individualHistoryList');
                //echo json_encode(array('stauts'=>FALSE,'message'=>$history));
                if (!empty($history)) { 
                    $history = json_decode($history,true);
                    
                    if (!empty($history[0]['startup_name'])) {                        
                         
                        foreach ($history as $hist) {
                            if($hist['startup_id']==Null){
                                $sql = "INSERT INTO sc_angel_individual_investment_history (id,angel_individual_id,startup_name,amount,created_date) "
                                        . "VALUE (:id,:angel_individual_id,:startup_name,:startup_amount,:date)";
                                $stmt = $db->prepare($sql);
                                $helper = new Helper();
                                $investor_id = $helper->guid4();
                                $stmt->bindParam('id', $investor_id);
                            }else{
                                $sql2 = "UPDATE sc_angel_individual_investment_history SET startup_name=:startup_name, amount=:startup_amount, updated_date=:date WHERE angel_individual_id = :angel_individual_id and id = :startup_id";    
                                $stmt = $db->prepare($sql2);
                                $stmt->bindParam('startup_id', $hist['startup_id']);
                            }
                            $stmt->bindParam('startup_name', $hist['startup_name']);
                            $stmt->bindParam('startup_amount', $hist['startup_amount']);
                            $stmt->bindParam('angel_individual_id', $id);                            
                            $stmt->bindParam('date', $date);
                            $stmt->execute();
                        }                   
                    }
                }                
            return true;
        }catch(PDOException $e){            
           return false;
        }    
    }
    
    function deleteHistory($request){        
        $individual_id = $request->post('id');
        $deleted_id = $request->post('deleted_id');
        $deleted_id = json_decode($deleted_id, true);
        try{
            foreach($deleted_id as $id){
                $query = "DELETE FROM sc_angel_individual_investment_history WHERE id = :id and angel_individual_id = :individual_id";
                $db = getDB();
                $stmt = $db->prepare($query);
                $stmt->bindParam("id", $id);
                $stmt->bindParam("individual_id", $individual_id);
                $stmt->execute();
            }    
            return true;
        }catch(PDOException $e){
            return false;
        }
    }

}

?>