<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'db.php';

class UsersProfile {

    function passwordChange($user_id, $old_password, $new_password) {
        try{
            $query = "UPDATE sc_users SET password = :new_password WHERE id = :user_id and password = :old_password";
            $db = getDB();
            $stmt = $db->prepare($query);
            $stmt->bindParam('user_id', $user_id);
            $stmt->bindParam('old_password', $old_password);
            $stmt->bindParam('new_password', $new_password);
            $stmt->execute();
            $db = null;
            return true;
        }
        catch (PDOException $e){
            return false;
        }
    }
    
    function passwordForgot($email,$password){
         $sql = "UPDATE sc_users SET password = :sc_password WHERE email = :sc_email";
        try {
            $db = getDB();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("sc_email", $email);
            $stmt->bindParam("sc_password", $password);
            $stmt->execute();            
            $db = null;
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function updateUserTokenExpire($email, $new_token) {
        $token_expire = date('Y-m-d H:i:s', strtotime("+7 days"));
        $date = date('Y-m-d H:i:s');
        try {
            $db = getDB();
            $stmt = $db->prepare("UPDATE sc_users SET token = :sc_token,token_expire = :sc_token_expire, updated_date = :sc_update_date where email = :sc_email");
            $stmt->bindParam("sc_email", $email);
            $stmt->bindParam("sc_token", $new_token);
            $stmt->bindParam("sc_token_expire", $token_expire);
            $stmt->bindParam("sc_update_date", $date);
            $stmt->execute();
            $db = null;
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }
    
    function userRegister($request){
        $fName = $request->post('first_name');
        $mName = $request->post('middle_name');
        $lName = $request->post('last_name');
        $sName = $request->post('short_name');
        $mobile = $request->post('mobile_no');
        $email = $request->post('email');
        $password = random_password();
        $pass = md5($password);
        $helper = new Helper();
        $user_id = $helper->guid4();
        $result = array();
        $sql = "INSERT INTO sc_users (id,first_name,middle_name, last_name, short_name,mobile_no,email,password,token,token_expire,created_date) VALUES (:sc_id, :sc_fname ,:sc_mname, :sc_lname, :sc_sname, :sc_mobile,:sc_email,:sc_password,:sc_token,:sc_token_expire,:sc_created_date)";
        try {
            $helper = new Helper();
            $user_id = $helper->guid4();
            $token = $helper->guid4();
            $date = date("Y-m-d H:m:s");
            $token_expire = date('Y-m-d H:i:s', strtotime("+7 days"));
            $db = getDB();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("sc_id", $user_id);
            $stmt->bindParam("sc_fname", $fName);
            $stmt->bindParam("sc_mname", $mName);
            $stmt->bindParam("sc_lname", $lName);
            $stmt->bindParam("sc_sname", $sName);
            $stmt->bindParam("sc_mobile", $mobile);
            $stmt->bindParam("sc_email", $email);
            $stmt->bindParam("sc_password", $pass);
            $stmt->bindParam("sc_token", $token);
            $stmt->bindParam("sc_token_expire", $token_expire);
            $stmt->bindParam("sc_created_date", $date);
            $stmt->execute();
            $db = null;
            $result = array("status"=> true, "user_id" => $user_id,"password"=>$password);
            return $result;
        } catch (PDOException $e){
            return $result = array("status"=>false);
        }
    }
    
    function deleteUserAcount($user_id){
        try{
            $query = "DELETE FROM sc_user WHERE id = sc_id ";
            $db = getDB();
            $stmt = $db->prepare($query);
            $stmt->bindParam("sc_id", $user_id);
            $stmt->execute();
            $db = null;
            return true;
        }catch(PDOException $e){
            return false;
        }    
    }
    
    function userLogin($uname,$pass){
        try{
            $db = getDB();
            $stmt = $db->prepare("SELECT id,user_type from sc_users WHERE email = :sc_email and password = :sc_pwd");
            $stmt->bindParam("sc_email", $uname);
            $stmt->bindParam("sc_pwd", $pass);
            $stmt->execute();
            $user_value = $stmt->fetch(PDO::FETCH_ASSOC);
            $db = null;
            return $user_value;
        }catch(PDOException $e){
            return false;
        }
    }

}
