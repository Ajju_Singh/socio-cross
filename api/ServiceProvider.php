<?php

//namespace DataBase;
require_once 'db.php';

/**
 * 
 * @author Admin
 */
class ServiceProvider {

    const USER_TYPE = "4";

    private $id;
    private $user_id;
    private $user_type;
    private $group_name;
    private $description;
    private $address;
    private $city;
    private $state;
    private $country;
    private $zip_code;
    private $fb_profile;
    private $ln_profile;
    private $url;
    private $about;
    private $profile_image;
    private $product_list = array();

    function __construct($serviceProvider = array()) {
        $this->id = isset($serviceProvider['id']) ? $serviceProvider['id'] : null;
        $this->user_id = isset($serviceProvider['user_id']) ? $serviceProvider['user_id'] : null;
        $this->user_type = isset($serviceProvider['user_type']) ? $serviceProvider['user_type'] : null;
        $this->group_name = isset($serviceProvider['group_name']) ? $serviceProvider['group_name'] : null;
        $this->description = isset($serviceProvider['description']) ? $serviceProvider['description'] : null;
        $this->address = isset($serviceProvider['address']) ? $serviceProvider['address'] : null;
        $this->city = isset($serviceProvider['city']) ? $serviceProvider['city'] : null;
        $this->state = isset($serviceProvider['state']) ? $serviceProvider['state'] : null;
        $this->country = isset($serviceProvider['country']) ? $serviceProvider['country'] : null;
        $this->zip_code = isset($serviceProvider['zip_code']) ? $serviceProvider['zip_code'] : null;
        $this->fb_profile = isset($serviceProvider['fb_profile']) ? $serviceProvider['fb_profile'] : null;
        $this->ln_profile = isset($serviceProvider['ln_profile']) ? $serviceProvider['ln_profile'] : null;
        $this->url = isset($serviceProvider['url']) ? $serviceProvider['url'] : null;
        $this->about = isset($serviceProvider['about']) ? $serviceProvider['about'] : null;
        $this->profile_image = isset($serviceProvider['profile_image']) ? $serviceProvider['profile_image'] : null;

        if (isset($serviceProvider['product_list']) && $serviceProvider['product_list']) {
            $this->product_list = ServiceProviderProduct::products($serviceProvider['product_list']);
        } else {
            $this->product_list = null;
        }
    }

    function getJsonData() {
        $var = get_object_vars($this);
        foreach ($var as &$value) {
            if (is_object($value) && method_exists($value, 'getJsonData')) {
                $value = $value->getJsonData();
            }
        }
        return $var;
    }

}

class ServiceProviderProduct {

    private $product_id;
    private $product_name;
    private $product_category;
    private $product_description;
    
    public static function products($products) {
        $serviceProducts = array();
        foreach ($products as $p) {
            $serviceProducts[] = array(
                'product_id' => $p['id'],
                'product_name' => $p['name'],
                'product_category' => $p['category'],
                'product_description' => $p['product_description']
            );
        }
        return $serviceProducts;
    }

}

class ServiceProviderController {

    function getByUserId($userId) {
        $query = "SELECT ssp.*, u.user_type FROM sc_service_provider ssp INNER JOIN sc_users u ON ssp.user_id = u.id WHERE u.id = :userId";
        $db = getDB();
        $stmt = $db->prepare($query);
        $stmt->bindParam("userId", $userId);
        $stmt->execute();
        $serviceProvider = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($serviceProvider) {
            $query2 = "SELECT id,name,category,product_description FROM sc_service_provider_products WHERE service_provider_id = :spid";
            $stmt = $db->prepare($query2);
            $stmt->bindParam("spid", $serviceProvider['id']);
            $stmt->execute();
            $serviceProvider['product_list'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return new ServiceProvider($serviceProvider);
        }
        return null;
    }
    
    function userExists($userId){
        $query = "SELECT user_id FROM sc_service_provider WHERE user_id = :userId";
        $db = getDB();
        $stmt = $db->prepare($query);
        $stmt->bindParam("userId", $userId);
        $stmt->execute();
        $userRegistered = $stmt->fetch(PDO::FETCH_ASSOC);
        if($userRegistered){
            return false;
        }else{
            return true;
        }
    }
    
    function registerService($request,$image_name){
        $helper = new Helper();
        try{
            $user_id = $request->post('user_id');
            $group_name = $request->post('group_name');
            $description = $request->post('description');
            $address = $request->post('address');
            $city = $request->post('city');
            $state = $request->post('state');
            $country = $request->post('country');
            $zipCode = $request->post('pin');
            $fbProfile = $request->post('fb_link');
            $lnProfile = $request->post('ln_link');
            $url = $request->post('url');
            $about = $request->post('about');            
            
            $sql = "INSERT INTO sc_service_provider (id,user_id,group_name, description,profile_image,address,city,state,country,zip_code,fb_profile,ln_profile,url,about,created_date) "
                        . "VALUES (:sc_id,:sc_user_id,:sc_group_name,:sc_description,:sc_logo,:sc_address,:sc_city,:sc_state,:sc_country,:sc_zip_code,:sc_fb_profile,:sc_ln_profile,:sc_url,:sc_about,:sc_created_date)";

            $service_id = $helper->guid4();
            $date = date('Y-m-d H:i:s');
            $db = getDB();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("sc_id", $service_id);
            $stmt->bindParam("sc_user_id", $user_id);
            $stmt->bindParam("sc_group_name", $group_name);
            $stmt->bindParam("sc_description", $description);
            $stmt->bindParam("sc_logo", $image_name);
            $stmt->bindParam("sc_address", $address);
            $stmt->bindParam("sc_city", $city);
            $stmt->bindParam("sc_state", $state);
            $stmt->bindParam("sc_country", $country);
            $stmt->bindParam("sc_zip_code", $zipCode);
            $stmt->bindParam("sc_fb_profile", $fbProfile);
            $stmt->bindParam("sc_ln_profile", $lnProfile);
            $stmt->bindParam("sc_url", $url);
            $stmt->bindParam("sc_about", $about);
            $stmt->bindParam("sc_created_date", $date);
            $stmt->execute();

            $products = $request->post('product_list');
            if (!empty($products)) {
                $products = json_decode($products, true);
                if (!empty($products[0]['product_name'])) {
                    $sql = 'INSERT INTO sc_service_provider_products (id,service_provider_id,name,category,product_description,created_date) VALUES ';
                    $insertQuery = array();
                    $insertData = array();

                    foreach ($products as $i => $p) {
                        $insertQuery[] = '(:id' . $i . ', :service_provider_id' . $i . ', :name' . $i . ', :category' . $i . ', :product_description' . $i . ', :created_date' . $i . ')';
                        $insertData['id' . $i] = $helper->guid4();
                        $insertData['service_provider_id' . $i] = $service_id;
                        $insertData['name' . $i] = $p['product_name'];
                        $insertData['category' . $i] = $p['product_category'];
                        $insertData['product_description' . $i] = $p['product_description'];
                        $insertData['created_date' . $i] = $date;
                    }

                    if (!empty($insertQuery)) {
                        $sql .= implode(', ', $insertQuery);
                        $stmt = $db->prepare($sql);
                        $stmt->execute($insertData);
                    }
                }
            }
            return true;
        }catch(PDOException $e){
           return false; 
        }
    }
    
    function updateService($request,$image_name){
        $id = $request->post('id');
        $user_id = $request->post('user_id');
        $group_name = $request->post('group_name');
        $description = $request->post('description');        
        $address = $request->post('address');
        $city = $request->post('city');
        $state = $request->post('state');
        $country = $request->post('country');
        $zipCode = $request->post('pin');
        $fbProfile = $request->post('fb_link');
        $lnProfile = $request->post('ln_link');
        $url = $request->post('url');
        $about = $request->post('about');
        $product_list = $request->post('product_list');
        
        try {
            if($image_name == null){                
                $image_name = $request->post('profile_image');                               
            }
            
            $query = "UPDATE sc_service_provider SET group_name = :groupName, description = :description, profile_image = :imageName, address = :address, city = :city, state = :state, country = :country, zip_code = :zipCode, fb_profile = :fbProfile, ln_profile = :lnProfile, url = :url,"
                    . " about = :about, updated_date = :updateDate"
                    . " WHERE user_id = :userId";

            

            $db = getDB();
            $date = date('Y-m-d H:i:s');
            $stmt = $db->prepare($query);            
            $stmt->bindParam("userId", $user_id);
            $stmt->bindParam("groupName", $group_name);
            $stmt->bindParam("description", $description);
            $stmt->bindParam("imageName", $image_name);
            $stmt->bindParam("address", $address);
            $stmt->bindParam("city", $city);
            $stmt->bindParam("state", $state);
            $stmt->bindParam("zipCode", $zipCode);
            $stmt->bindParam("country", $country);
            $stmt->bindParam("fbProfile", $fbProfile);
            $stmt->bindParam("lnProfile", $lnProfile);
            $stmt->bindParam("url", $url);
            $stmt->bindParam("about", $about);
            $stmt->bindParam("updateDate", $date);
            $stmt->execute();
            
            if (!empty($product_list)) {                
                    $product_list = json_decode($product_list, true);
                    //var_dump($product_list);
                    if (!empty($product_list[0]['product_name'])) { 
                        foreach ($product_list as $product) {
                            if($product['product_id']==null){
                                $sql2 = 'INSERT INTO sc_service_provider_products (id,service_provider_id,name,category,product_description,created_date) '
                                        . 'VALUES (:id,:service_id,:name,:category,:description,:date)';
                                $stmt = $db->prepare($sql2);
                                $helper = new Helper();
                                $service_id = $helper->guid4();
                                $stmt->bindParam('id', $service_id);
                            }else{
                                $sql = "UPDATE sc_service_provider_products SET name = :name, category=:category, product_description=:description, updated_date=:date "
                                . "WHERE service_provider_id = :service_id and id = :product_id";
                                $stmt = $db->prepare($sql);
                                $stmt->bindParam('product_id', $product['product_id']);
                            }                            
                            $stmt->bindParam('name', $product['product_name']);
                            $stmt->bindParam('category', $product['product_category']);
                            $stmt->bindParam('service_id', $id);                            
                            $stmt->bindParam('description', $product['product_description']);                           
                            $stmt->bindParam('date', $date);                            
                            $stmt->execute();
                            //var_dump($r,$product);
                        }                   
                    }
                }
            
            $db = null;

            return true;
        } catch (PDOException $e) {
            //return false;
            echo $e->getMessage();
        }
    }
    
    function deleteProducts($request){
        $deleted_id = $request->post('deleted_id');
        $service_id = $request->post('id');
        $deleted_id = json_decode($deleted_id, true);        
        try{
            foreach($deleted_id as $id){
                $query = "DELETE FROM sc_service_provider_products WHERE id = :id and service_provider_id = :service_id";
                $db = getDB();
                $stmt = $db->prepare($query);
                $stmt->bindParam("id", $id);
                $stmt->bindParam("service_id", $service_id);
                $stmt->execute();
            }    
            return true;
        }catch(PDOException $e){
            return false;
        }
    }

}

?>