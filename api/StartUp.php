<?php

//namespace DataBase;
require_once 'db.php';

/**
 *
 * @author Admin
 */
class StartUp {

    const USER_TYPE = "1";

    private $id;
    private $user_id;
    private $user_type;
    private $startup_name;
    private $url;
    private $address;
    private $city;
    private $pin;
    private $fb_link;
    private $ln_link;
    private $google_link;
    private $state;
    private $country;
    private $year_founded;
    private $registered;
    private $year_registered;
    private $registered_as;
    private $stage;
    private $sector;
    private $category;
    private $short_description;
    private $detailed_description;
    private $file_name;
    private $target_customer_profile;
    private $unit_price;
    private $contributors;
    private $num_customers;
    private $num_users;
    private $video_url;
    private $presentation_url;
    private $brochure_url;
    private $play_store_url;
    private $istore_url;
    private $other_market_place;
    private $profile_image;
    private $investment_amount;
    private $link_busniess_plan;
    private $mentors_sectors;
    private $location;
    private $select_products;

    function __construct($startUp = array()) {
        $this->id = isset($startUp['id']) ? $startUp['id'] : null;
        $this->user_id = isset($startUp['user_id']) ? $startUp['user_id'] : null;
        $this->user_type = isset($startUp['user_type']) ? $startUp['user_type'] : null;
        $this->startup_name = isset($startUp['startup_name']) ? $startUp['startup_name'] : null;
        $this->url = isset($startUp['url']) ? $startUp['url'] : null;
        $this->address = isset($startUp['address']) ? $startUp['address'] : null;
        $this->city = isset($startUp['city']) ? $startUp['city'] : null;
        $this->pin = isset($startUp['zip_code']) ? $startUp['zip_code'] : null;
        $this->fb_link = isset($startUp['fb_profile']) ? $startUp['fb_profile'] : null;
        $this->ln_link = isset($startUp['ln_profile']) ? $startUp['ln_profile'] : null;
        $this->google_link = isset($startUp['google_profile']) ? $startUp['google_profile'] : null;
        $this->state = isset($startUp['state']) ? $startUp['state'] : null;
        $this->country = isset($startUp['country']) ? $startUp['country'] : null;
        $this->year_founded = isset($startUp['year_founded']) ? $startUp['year_founded'] : null;
        $this->registered = isset($startUp['registered']) ? $startUp['registered'] : null;
        $this->year_registered = isset($startUp['year_registered']) ? $startUp['year_registered'] : null;
        $this->registered_as = isset($startUp['registered_as']) ? $startUp['registered_as'] : null;
        $this->stage = isset($startUp['stage']) ? $startUp['stage'] : null;
        $this->sector = isset($startUp['sectors']) ? $startUp['sectors'] : null;
        $this->category = isset($startUp['category']) ? $startUp['category'] : null;
        $this->short_description = isset($startUp['short_description']) ? $startUp['short_description'] : null;
        $this->detailed_description = isset($startUp['detailed_description']) ? $startUp['detailed_description'] : null;
        $this->file_name = isset($startUp['file_name']) ? $startUp['file_name'] : null;
        $this->target_customer_profile = isset($startUp['target_customer_profile']) ? $startUp['target_customer_profile'] : null;
        $this->unit_price = isset($startUp['unit_price']) ? $startUp['unit_price'] : null;
        $this->contributors = isset($startUp['contributors']) ? $startUp['contributors'] : null;
        $this->num_customers = isset($startUp['no_of_customers']) ? $startUp['no_of_customers'] : null;
        $this->num_users = isset($startUp['no_of_users']) ? $startUp['no_of_users'] : null;
        $this->video_url = isset($startUp['video_url']) ? $startUp['video_url'] : null;
        $this->presentation_url = isset($startUp['presentation_url']) ? $startUp['presentation_url'] : null;
        $this->brochure_url = isset($startUp['brochure_url']) ? $startUp['brochure_url'] : null;
        $this->play_store_url = isset($startUp['play_store_url']) ? $startUp['play_store_url'] : null;
        $this->istore_url = isset($startUp['istore_url']) ? $startUp['istore_url'] : null;
        $this->other_market_place = isset($startUp['other_market_place']) ? $startUp['other_market_place'] : null;
        $this->profile_image = isset($startUp['logo_name']) ? $startUp['logo_name'] : null;
        $this->investment_amount = isset($startUp['investment_amount']) ? $startUp['investment_amount'] : null;
        $this->link_busniess_plan = isset($startUp['link_busniess_plan']) ? $startUp['link_busniess_plan'] : null;
        $this->mentors_sectors = isset($startUp['mentors_sectors']) ? $startUp['mentors_sectors'] : null;
        $this->location = isset($startUp['location']) ? $startUp['location'] : null;
        $this->select_products = isset($startUp['select_products']) ? $startUp['select_products'] : null;
    }

    function getJsonData() {
        $var = get_object_vars($this);
        foreach ($var as &$value) {
            if (is_object($value) && method_exists($value, 'getJsonData')) {
                $value = $value->getJsonData();
            }
        }
        return $var;
    }

}

class StartUpController {

    function getByUserId($userId) {
        $query = "SELECT p.*, u.user_type FROM sc_startup_profile p INNER JOIN sc_users u ON u.id = p.user_id WHERE u.id = :userId";
        $db = getDB();
        $stmt = $db->prepare($query);
        $stmt->bindParam("userId", $userId);
        $stmt->execute();
        $startUp = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($startUp) {
            return new StartUp($startUp);
        }
        return null;
    }
    
    function userExists($userId){
        $query = "SELECT user_id FROM sc_startup_profile WHERE user_id = :userId";
        $db = getDB();
        $stmt = $db->prepare($query);
        $stmt->bindParam("userId", $userId);
        $stmt->execute();
        $userRegistered = $stmt->fetch(PDO::FETCH_ASSOC);
        if($userRegistered){
            return false;
        }else{
            return true;
        }
    }
    
    function registerStartup($request,$image_name,$fileName){

        $user_id = $request->post('user_id');
        $startup_name = $request->post('startup_name');
        $url = $request->post('url');
        $address = $request->post('address');
        $city = $request->post('city');
        $state = $request->post('state');
        $country = $request->post('country');
        $pin = $request->post('pin');
        $fb_link = $request->post('fb_link');
        $ln_link = $request->post('ln_link');
        $google_link = $request->post('google_link');
        $year_founded = $request->post('year_founded');
        $registered = $request->post('registered');
        $year_registered = $request->post('year_registered');
        $registered_as = $request->post('registered_as');
        $stage = $request->post('stage');
        $sectors = $request->post('sector');
        $category = $request->post('category');
        $short_des = $request->post('short_description');
        $detailed_des = $request->post('detailed_description');
        $target_customer_profile = $request->post('target_customer_profile');
        $unit_price = $request->post('unit_price');
        $contributors = $request->post('contributors');
        $num_customers = $request->post('num_customers');
        $num_users = $request->post('num_users');
        $video_url = $request->post('video_url');
        $presentation_url = $request->post('presentation_url');
        $brochure_url = $request->post('brochure_url');
        $play_store_url = $request->post('play_store_url');
        $istore_url = $request->post('istore_url');
        $other_market_place = $request->post('other_market_place');
        $investment_amount = $request->post('investment_amount');
        $link_busniess_plan = $request->post('link_busniess_plan');
        $mentors_sectors = $request->post('mentors_sectors');
        $location = $request->post('location');
    //    $location_latitude          = $request->post('location_latitude');
    //    $location_longitude         = $request->post('location_longitude');
        $select_products = $request->post('select_products');
        
        $helper = new Helper();                    
        $id = $helper->guid4();
        $date = date('Y-m-d H:i:s');
        
        try{
            $query  = "INSERT INTO sc_startup_profile (id,user_id,startup_name,url,address,city,state,country,zip_code,fb_profile,ln_profile,google_profile,year_founded,registered,year_registered,registered_as,stage,sectors,category,short_description,detailed_description,file_name,target_customer_profile,unit_price,contributors,no_of_customers,no_of_users,logo_name,video_url,presentation_url,brochure_url,play_store_url,istore_url,other_market_place,investment_amount,link_busniess_plan,mentors_sectors,location,select_products,created_date) "
                        ." VALUES  (:id,:userId,:startupName,:url,:address,:city,:state,:country,:zipCode,:fbLink,:lnLink,:googleLink,:yearFounded,:registered,:yearRegistered,:registeredAs,:stage,:sectors,:category,:shortDes,:detailedDes,:fileName,:targetCustomerProfile,:unitPrice,:contributors,:numCustomers,:numUsers,:logoName,:videoUrl,:presentationUrl,:brochureUrl,:playStoreUrl,:istoreUrl,:otherMarketPlace,:investmentAmount,:linkBusniessPlan,:mentorsSectors,:location,:selectProducts,:createdDate)";
            $db     = getDB();
            $stmt   = $db->prepare($query);
            $stmt->bindParam("id", $id);
            $stmt->bindParam("userId", $user_id);
            $stmt->bindParam("startupName", $startup_name);
            $stmt->bindParam("url", $url);
            $stmt->bindParam("address", $address);
            $stmt->bindParam("city", $city);
            $stmt->bindParam("state", $state);
            $stmt->bindParam("country", $country);
            $stmt->bindParam("zipCode", $pin);
            $stmt->bindParam("fbLink", $fb_link);
            $stmt->bindParam("lnLink", $ln_link);
            $stmt->bindParam("googleLink", $google_link);
            $stmt->bindParam("yearFounded", $year_founded);
            $stmt->bindParam("registered", $registered);
            $stmt->bindParam("yearRegistered", $year_registered);
            $stmt->bindParam("registeredAs", $registered_as);
            $stmt->bindParam("stage", $stage);
            $stmt->bindParam("sectors", $sectors);
            $stmt->bindParam("category", $category);
            $stmt->bindParam("shortDes", $short_des);
            $stmt->bindParam("detailedDes", $detailed_des);
            $stmt->bindParam("fileName", $fileName);
            $stmt->bindParam("targetCustomerProfile", $target_customer_profile);
            $stmt->bindParam("unitPrice", $unit_price);
            $stmt->bindParam("contributors", $contributors);
            $stmt->bindParam("numCustomers", $num_customers);
            $stmt->bindParam("numUsers", $num_users);
            $stmt->bindParam("logoName", $image_name);
            $stmt->bindParam("videoUrl", $video_url);
            $stmt->bindParam("presentationUrl", $presentation_url);
            $stmt->bindParam("brochureUrl", $brochure_url);
            $stmt->bindParam("playStoreUrl", $play_store_url);
            $stmt->bindParam("istoreUrl", $istore_url);
            $stmt->bindParam("otherMarketPlace", $other_market_place);
            $stmt->bindParam("investmentAmount", $investment_amount);
            $stmt->bindParam("linkBusniessPlan",$link_busniess_plan);
            $stmt->bindParam("mentorsSectors", $mentors_sectors);
            $stmt->bindParam("location", $location);
            $stmt->bindParam("selectProducts", $select_products);
            $stmt->bindParam("createdDate", $date);//            
            $stmt->execute();        
            $db     = null;
            
            return true;
        } catch (PDOException $e){            
            echo $e->getMessage();
//            return false;
        }    
    }

    function updateStartupFounder($request,$image_name,$file_name){
        
        $user_id = $request->post('user_id');
        $startup_name = $request->post('startup_name');
        $url = $request->post('url');
        $address = $request->post('address');
        $city = $request->post('city');
        $state = $request->post('state');
        $country = $request->post('country');
        $pin = $request->post('pin');
        $fb_link = $request->post('fb_link');
        $ln_link = $request->post('ln_link');
        $google_link = $request->post('google_link');
        $year_founded = $request->post('year_founded');
        $registered = $request->post('registered');
        $year_registered = $request->post('year_registered');
        $registered_as = $request->post('registered_as');
        $stage = $request->post('stage');
        $sectors = $request->post('sector');
        $category = $request->post('category');
        $short_des = $request->post('short_description');
        $detailed_des = $request->post('detailed_description');
        $target_customer_profile = $request->post('target_customer_profile');
        $unit_price = $request->post('unit_price');
        $contributors = $request->post('contributors');
        $num_customers = $request->post('num_customers');
        $num_users = $request->post('num_users');
        $video_url = $request->post('video_url');
        $presentation_url = $request->post('presentation_url');
        $brochure_url = $request->post('brochure_url');
        $play_store_url = $request->post('play_store_url');
        $istore_url = $request->post('istore_url');
        $other_market_place = $request->post('other_market_place');
        $investment_amount = $request->post('investment_amount');
        $link_busniess_plan = $request->post('link_busniess_plan');
        $mentors_sectors = $request->post('mentors_sectors');
        $location = $request->post('location');
    //    $location_latitude          = $request->post('location_latitude');
    //    $location_longitude         = $request->post('location_longitude');
        $save_file = $request->post('file_name');
        $select_products = $request->post('select_products');
        $delete_file = $request->post('deleted_fileName_list');
        $delete_file = json_decode($delete_file, true);
        //echo json_encode(array('status'=>false,'message'=>array($file_name,$save_file,$delete_file)));die();
        if($image_name == null){
            $image_name = $request->post('profile_image');
        }
        //var_dump($save_file);die();
        try {
            
            if($delete_file != Null){
                $files = explode(",", $save_file);
                $update_file = array_diff($files, $delete_file);
                if(empty($update_file)){
                    $update_file = Null;
                }else{
                    $update_file = implode(",", $update_file);
                }    
            }else{
                $update_file = $save_file;
            }
            // echo json_encode(array('status'=>false,'message'=>$update_file));die();
//            if($file_name != Null){
//                if(!empty($update_file) && $update_file != Null){
//                    $update_file =$update_file.",". $file_name;
//                }elseif(isset ($file_name) && $update_file != Null &&!empty($update_file) && isset($save_file) && $save_file != 'Null'){                    
//                    $update_file = $file_name.",".$save_file;
//                }else{
//                    $update_file = $file_name ;
//                }
//            }elseif (!empty ($update_file)&& $update_file != Null ) {                
//                $update_file = $update_file.",".$file_name;                
//            }elseif($file_name == null && $update_file == Null ){
//                $update_file = $save_file;                            
//            }elseif($file_name == null && empty ($update_file)){
//                $update_file = Null;
//            }else{
//                $update_file = $file_name;
//            }   
           // echo json_encode(array('status'=>false,'message'=>array($file_name,$update_file,$save_file,$delete_file)));die();
            if($file_name != Null && ($update_file == Null || $update_file == 'null')){
                $update_file = $file_name;                
            }elseif($file_name != Null && ($update_file != Null || $update_file != 'null')){
                $update_file = $file_name.",".$update_file;
            }elseif($file_name == Null && ($update_file != Null || $update_file != 'null')){
                $update_file;
            }else{
                $update_file;
            }
            //echo json_encode(array('status'=>false,'message'=>$update_file));die();
            $query2 = "UPDATE sc_startup_profile SET startup_name=:startup_name, url=:url, address=:address, 
			city=:city, state=:state, country=:country, zip_code=:zip_code, fb_profile=:fb_profile,
			ln_profile=:ln_profile, google_profile=:google_profile, year_founded=:year_founded,
			registered=:registered, year_registered=:year_registered, registered_as=:registered_as,
			stage=:stage, sectors=:sectors, category=:category, short_description=:short_description,
			detailed_description=:detailed_description, file_name=:file_name, target_customer_profile=:target_customer_profile,
			unit_price=:unit_price, contributors=:contributors, no_of_customers=:no_of_customers,
			no_of_users=:no_of_users, logo_name=:logo_name, video_url=:video_url,
			presentation_url=:presentation_url, brochure_url=:brochure_url, play_store_url=:play_store_url,
			istore_url=:istore_url, other_market_place=:other_market_place, investment_amount=:investment_amount,
			link_busniess_plan=:link_busniess_plan, mentors_sectors=:mentors_sectors, location=:location,
			select_products=:select_products, updated_date=:updated_date 
                        WHERE user_id = :user_id";
            
            $db   = getDB();
            $date = date('Y-m-d H:i:s');
            $stmt = $db->prepare($query2);            
            $stmt->bindParam('user_id', $user_id);
            $stmt->bindParam('startup_name', $startup_name);
            $stmt->bindParam('url', $url);
            $stmt->bindParam('address', $address);
            $stmt->bindParam('city', $city);
            $stmt->bindParam('state', $state);
            $stmt->bindParam('country', $country);
            $stmt->bindParam('zip_code', $pin);
            $stmt->bindParam('fb_profile', $fb_link);
            $stmt->bindParam('ln_profile', $ln_link);
            $stmt->bindParam('google_profile', $google_link);
            $stmt->bindParam('year_founded', $year_founded);
            $stmt->bindParam('registered', $registered);
            $stmt->bindParam('year_registered', $year_registered);
            $stmt->bindParam('registered_as', $registered_as);
            $stmt->bindParam('stage', $stage);
            $stmt->bindParam('sectors', $sectors);
            $stmt->bindParam('category', $category);
            $stmt->bindParam('short_description', $short_des);
            $stmt->bindParam('detailed_description', $detailed_des);
            $stmt->bindParam('file_name', $update_file);
            $stmt->bindParam('target_customer_profile', $target_customer_profile);
            $stmt->bindParam('unit_price', $unit_price);
            $stmt->bindParam('contributors', $contributors);
            $stmt->bindParam('no_of_customers', $num_customers);
            $stmt->bindParam('no_of_users', $num_users);
            $stmt->bindParam('logo_name', $image_name);
            $stmt->bindParam('video_url', $video_url);
            $stmt->bindParam('presentation_url', $presentation_url);
            $stmt->bindParam('brochure_url', $brochure_url);
            $stmt->bindParam('play_store_url', $play_store_url);
            $stmt->bindParam('istore_url', $istore_url);
            $stmt->bindParam('other_market_place', $other_market_place);
            $stmt->bindParam('investment_amount', $investment_amount);
            $stmt->bindParam('link_busniess_plan', $link_busniess_plan);
            $stmt->bindParam('mentors_sectors', $mentors_sectors);
            $stmt->bindParam('location', $location);
            $stmt->bindParam('select_products', $select_products);
            $stmt->bindParam('updated_date', $date);
            $stmt->execute();
            $db = null;
            return true;
        } catch (PDOException $e) {
            return false;            
        }
    }
}

?>